//Used to support the bloodhound lib, as it has jquery as a dependency
module.exports = (function() {
  angular.extend(angular, {
    map:  function( elems, callback, arg ) {
      var value, key, ret = [],
        i = 0,
        length = elems.length,
        // jquery objects are treated as arrays
        isArray =  length !== undefined && typeof length === "number" && ( ( length > 0 && elems[ 0 ] && elems[ length -1 ] ) || length === 0 || angular.isArray( elems ) ) ;

      // Go through the array, translating each of the items to their
      if ( isArray ) {
        for ( ; i < length; i++ ) {
          value = callback( elems[ i ], i, arg );

          if ( value != null ) {
            ret[ ret.length ] = value;
          }
        }

      // Go through every key on the object,
      } else {
        for ( key in elems ) {
          value = callback( elems[ key ], key, arg );

          if ( value != null ) {
            ret[ ret.length ] = value;
          }
        }
      }

      // Flatten any nested arrays
      return ret.concat.apply( [], ret );
   },
  filter: function( elems, callback, inv ) {
		var ret = [], retVal;
		inv = !!inv;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( var i = 0, length = elems.length; i < length; i++ ) {
			retVal = !!callback( elems[ i ], i );
			if ( inv !== retVal ) {
				ret.push( elems[ i ] );
			}
		}

		return ret;
	}
  });
})();
