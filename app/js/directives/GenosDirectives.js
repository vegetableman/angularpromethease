/*jshint multistr: true */
var genosDirectives;

module.exports = genosDirectives = angular.module('GenosDirectives', []);

require('../../../vendor_libs/sigma.js');
require('../../../vendor_libs/Chart.js');

var React = require('react'),
	BlockView = React.createFactory(require('../components/BlockView.jsx')),
	TableView = React.createFactory(require('../components/TableView.jsx'));

//dialogs
genosDirectives.directive('modalDialog', function() {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		template: '<div class="modal-dialog"><div class="modal-content" ng-transclude></div><div class="modal-arrow"></div></div>',
		link: function(scope, element, attrs, controller) {
			var leftOffset,
				elementWidth,
				cvalues,
				setDialogPos = function(values) {
					cvalues = values;

					if(!values[0] === undefined) return;

					if(attrs.isDisplayed && attrs.index && attrs.index === values[0]) {
						attrs.isDisplayed = false;
						angular.element(element).removeClass('fade');
					}
					else if(attrs.index && attrs.index === values[0]) {

						//cache width
						if(!elementWidth)
							elementWidth = element[0].getBoundingClientRect().width;

						var rightOffset = values[2] + elementWidth;

						//cache left offset
						if(rightOffset > window.innerWidth) {
							var diff = rightOffset - window.innerWidth;
							leftOffset =  values[2] - diff;
							//arrow
						  angular.element(element).children().eq(1).css('left', diff - +(attrs.leftoffset || 0) + 'px');
						} else {
							leftOffset = values[2] + +attrs.leftoffset;
						}

						angular.element(element).addClass('modal-fade');
						attrs.isDisplayed = true;

						angular.element(element).css('top', (values[1] + +(attrs.topoffset || 0)) + 'px');
						angular.element(element).css('left', leftOffset + 'px');
						scope.$eval(attrs.show);
					}
					else {
						angular.element(element).removeClass('modal-fade');
						attrs.isDisplayed = false;
					}
				};

			var deregister = scope.$watchCollection('[dialog.index, dialog.top, dialog.left, dialog.incr]', function(values) {
				setDialogPos(values);
			});

			scope.$on('destroy', function() {
				deregister();
			});
		}
	};
});


//dialog trigger
genosDirectives.directive('modalDialogItem', function(safeApply) {
	return {
		link: function(scope, element, attrs, controller) {
			var incr = 0;
			angular.element(element).unbind('click').bind('click', function(e) {
				//using closest - http://css-tricks.com/dangers-stopping-event-propagation/
				// e.stopPropagation();

				if(scope.dialog.index === attrs.index) {
					safeApply(scope, function() {
						scope.dialog.index = undefined;
						scope.main.open = false;
					});
				} else {
					safeApply(scope, function() {
						scope.main.open = true;
						scope.incr = ++incr;
						scope.dialog.index = attrs.index;
						scope.dialog.top = element[0].getBoundingClientRect().top+ element[0].getBoundingClientRect().height + -(document.querySelector('header').getBoundingClientRect()['top']);
						scope.dialog.left = element[0].getBoundingClientRect().left;
					});
				}
				if(attrs.itemClick)
					scope.$eval(attrs.itemClick);
			});
		}
	};
});


genosDirectives.directive('pullDown', function() {
	return {
		link: function(scope, element, attrs) {
			var header = angular.element(document.querySelector('header')),
				el = element[0];

			element.bind('click', function() {
				if (header[0].getBoundingClientRect()['top'] < 0) {
					header.addClass('slide--reset').removeClass('slide--up');
				}
			});

			var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
        observer;

        	observer = new MutationObserver(function(mutations) {
		        mutations.forEach(function(m) {
		        	if(header.hasClass('slide--up')) {
		        		el.style.display = 'block';
		        	}
		        	else {
		        		el.style.display = 'none';
		        	}
		        });
		    });

		    observer.observe(header[0], {
		        attributes: true
		    });

		}
	};
});


//has all window events defined
genosDirectives.directive('docEvents', ['$window', 'safeApply', 'debounce', function($window, safeApply, debounce) {
	return {
		link: function(scope, element, attrs) {
			var header = $('header');
			var height = window.innerHeight;
	        var modalDialog = $('.modal-dialog');
			var closeDialog = function() {
				if(scope.dialog) {
					scope.dialog.index = undefined;
					scope.$digest();
				}
			};

			//ESC event
			angular.element(element).bind('keyup', function(e) {
				if (e.keyCode === 27) {
					scope.$broadcast('closeMobileMenu');
					closeDialog();
				}
			});

			//Hide modal
		    angular.element($window).bind('scroll', function() {
		    	debounce(function() {
		    		if (!scope.main.open) return;

		    		var header = document.querySelector('header');
					if (angular.element(header).hasClass('slide--up')) {
						modalDialog.each(function(index, item) {
							if ($(item).hasClass("modal-fade")) {
								var scope = angular.element(item).scope();
								safeApply(scope, function() {
									scope.dialog.index = undefined;
								});
							}
						});
					}
				}, 250)();
		    });

			//click on body
			angular.element($window).unbind('click').bind('click', function(e) {
				if(!$(e.target).closest('.mobile-menu').length 
					&& !$(e.target).closest('.modal-dialog').length
					&& !$(e.target).closest('.genos-controls').length
					&& typeof $(e.target).attr('modal-dialog-item') === 'undefined'
					&& typeof $(e.target).parent().attr('modal-dialog-item') === 'undefined') {
					scope.$broadcast('closeMobileMenu');
					closeDialog();
				}
			});

			scope.$on('itemHeight', function(event, value) {
				window.setTimeout(function() {
					var item = angular.element($('.genos-item').eq(value)[0]);
					item.css('min-height', '');
					var height = item[0].offsetHeight;
					item.data('height', height);
					item.css('min-height', height + 'px');
				}, 50)
			});
		}
	};
}]);

genosDirectives.directive('blockView', function(collectionService) {
	return  {
		scope: true,
		controller: function($scope) {
			var that = this;
			that.lastPage = false;
			that.collection = [];
			that.collectionChange = false;

			that.remountComponent = function(rows) {
				React.unmountComponentAtNode(that.el);
				React.render(
					BlockView({
						collection: that.collection,
						limit: collectionService.getLimit(),
						rows: rows,
						showBoring: that.showBoring,
						showFirstPage: that.showFirstPage,
						showLastPage: that.showLastPage,
						setTag: that.filterByTag,
						setClinvar: that.filterByClinvar,
						showPage: that.showPage,
						lastPage: that.lastPage,
						onResize: that.onResize,
						pageInputValue: that.pageInputValue,
						loaderTop: document.querySelector('.loader-top-scroll'),
						loaderBottom: document.querySelector('.loader-bottom-scroll'),
						scrollInfo: document.querySelector('.scroll-up-info'),
						paginationContainer: document.querySelector('.pagination-container')
					}),
				   that.el
				);

				document.querySelector('.view-loader').style.visibility = 'hidden';
			};

			that.setCollection = function(collection) {
				that.collection = collection;
			};

			that.setEl = function(el) {
				that.el = el;
			}

			that.filterByTag = function(tag) {
				$scope.$emit('filterByTag', tag);
			};

			that.filterByClinvar = function(clinvar) {
				$scope.$emit('filterByClinvar', clinvar);
			};

			that.showBoring = function(index) {
				$scope.$emit('showBoring', index);
			};

			that.showPage = function(rerender, top, value) {
				that.pageInputValue = value;

				if(rerender) {
					that.remountComponent();
					window.scrollTo(0,0);
				} else {
					window.setTimeout(function() {
						window.scrollTo(0, top);
					}, 50);
				}
			};

			that.showFirstPage = function(rerender) {
				that.lastPage = false;
				that.pageInputValue = null;

				if(rerender) {
					that.remountComponent();
				}
				window.scrollTo(0,0);
			};

			that.showLastPage = function(rerender) {
				that.lastPage = true;
				that.pageInputValue = null;

				if(rerender) {
					that.remountComponent();
				}
				window.scrollTo(0, document.body.scrollHeight);
			};

			that.onResize = function(totalRows) {
				console.log('onResize');
				that.remountComponent(totalRows);
			};

			that.setCollectionChange = function(value) {
				that.collectionChange = value;
			}

			that.hasCollectionChanged = function() {
				return that.collectionChange;
			}

		},
		link: function(scope, element, attrs, controller) {
			var el = element[0];

			controller.setEl(el);

			scope.$watchCollection(function() { 
				return collectionService.getActiveCollection();
			}, function(newValue) {
				//  || scope.main.doShowGraph
				if(scope.main.viewIndex !== 0) {
					controller.setCollectionChange(true);
					controller.setCollection(newValue);
					return;
				}

				document.querySelector('.view-loader').style.visibility = 'visible';

				//reset page input
				controller.pageInputValue = null;

				if(newValue && newValue.length) {
					controller.setCollection(newValue);

					window.setTimeout(function() {
						controller.remountComponent();
						window.scrollTo(0,0);
					}, 50);
				}
				//collection empty
				else {
					document.querySelector('.view-loader').style.visibility = 'hidden';
					React.unmountComponentAtNode(el);
				}
			});

			//hacks
			scope.main.mountBlockView = function(force) {
				scope.$broadcast('enableBlockView');

				if(controller.hasCollectionChanged() || force) {
					controller.setCollectionChange(false);

					window.setTimeout(function() {
						controller.remountComponent();
						window.scrollTo(0,1);
					}, 50);
				}
				else {
					document.querySelector('.view-loader').style.visibility = 'hidden';
					window.setTimeout(function() {
						window.scrollTo(0,1);
					}, 10);
				}
			};
		}
	}  
});


genosDirectives.directive('tableView', function(collectionService) {
	return {
		scope: true,
		controller: function() {
			var that = this;

			that.collection = [];

			that.remountComponent = function() {
				React.unmountComponentAtNode(that.el);
				React.render(
					TableView({
						collection: that.collection,
						limit: collectionService.getLimit()*10,
						showFirstPage: that.showFirstPage,
						showLastPage: that.showLastPage,
						loaderBtn: document.querySelector('.load-more-btn'),
						loaderBottom: document.querySelector('.loader-bottom-scroll')
					}),
				   that.el
				);

				document.querySelector('.view-loader').style.visibility = 'hidden';
			};

			that.setEl = function(el) {
				that.el = el;
			}

			that.setCollection = function(collection) {
				that.collection = collection;
			}

			that.showFirstPage = function() {
				window.scrollTo(0,0);
			};

			that.showLastPage = function() {
				window.scrollTo(0, document.body.scrollHeight);
			};

			that.setCollectionChange = function(value) {
				that.collectionChange = value;
			};

			that.hasCollectionChanged = function() {
				return that.collectionChange;
			};

		},
		link: function(scope, element, attrs, controller) {
			var el = element[0],
				collection = [];

			controller.setEl(el);

			scope.$watchCollection(function() { 
				return collectionService.getActiveCollection();
			}, function(newValue) {
				if(scope.main.viewIndex !== 1 || scope.main.doShowGraph) {
					controller.setCollectionChange(true);
					controller.setCollection(newValue);
					return;
				}

				document.querySelector('.view-loader').style.visibility = 'visible';

				if(newValue && newValue.length) {
					controller.setCollection(newValue);
					controller.remountComponent();
				}
				else {
					document.querySelector('.view-loader').style.visibility = 'hidden';
					React.unmountComponentAtNode(el);
				}
			});

			scope.main.mountTableView = function() {
				if(controller.hasCollectionChanged()) {
					controller.setCollectionChange(false);

					window.setTimeout(function() {
						controller.remountComponent();
						window.scrollTo(0,0);
					}, 50);
				}
				else {
					document.querySelector('.view-loader').style.visibility = 'hidden';
				}
			};

			scope.main.disableTableView = function() {
				scope.$broadcast('disableTableView', true);
			};
		}
	}
});


genosDirectives.directive('loadMoreButton', function() {
   return {
	   scope: true,
	   link: function(scope, element, attrs, controller) {
			var limit = scope.page.limit;
			angular.element(element).bind('click', function() {
				var incr = parseInt(scope.page.limit) + parseInt(limit);
				var collection = scope.isSearchResult() ? scope.searchCollection : scope.genosCollection;
				if(collection && incr <= collection.length) {
					scope.page.limit = incr;
					scope.$digest();
				}
				else {
					angular.element(element).addClass('ng-hide');
				}
			});
		}
	}
});

//typeahead - inspired by https://github.com/eyston/ymusica and twitter typeahead
genosDirectives.directive('typeahead', function() {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			show: '&',
			items: '=',
			select: '&',
			term: "="
		},
		template: '<input class="typeahead-input" ng-model="term" type="search" autocomplete="off" ng-change="query()"><div class="cmb-list-container" ng-transclude></div>',
		controller: function($scope) {
			var self = this;
			var foundItems;
			var foundIndex = 0;
			var totalItems = 0;
			var selKey;

			this.select = function(item) {
				$scope.term = item.key;
				$scope.select({item: item});
			};

			this.activate = function(item) {
				$scope.active = item;
			};

			this.activateNextItem = function() {
				//for items found on search
				if(this.getFoundItems()) {
					var foundItems = this.getFoundItems();
					var index = foundItems.indexOf($scope.active);
					this.activate(foundItems[(index+1) % foundItems.length]);
				}
				else if($scope.items) {
					var index = $scope.items.indexOf($scope.active);
					this.activate($scope.items[(index + 1) % $scope.items.length]);
				}
			};

			this.activatePreviousItem = function() {
				if(this.getFoundItems()) {
					var foundItems = this.getFoundItems();
					var index = foundItems.indexOf($scope.active);
					this.activate(foundItems[index === 0 ? index : index - 1]);
				}
				else if($scope.items) {
					var index = $scope.items.indexOf($scope.active);
					this.activate($scope.items[index === 0 ? index : index - 1]);
				}
			};

			this.selectActive = function() {
				if($scope.active)
					this.select($scope.active);
			};

			this.reset = function() {
				$scope.select();
			}

			this.isActive = function(item) {
				return $scope.active === item;
			};

			this.setItemTop = function(top, index) {
				$scope.itemTop = [top, index];
			};

			this.setFoundItems = function(items) {
				if(!items) foundIndex = 0;
				foundItems = items;
			};

			this.getFoundItems = function() {
				return foundItems;
			};

			//total number of original items
			this.setTotal = function(value) {
				totalItems = value;
			};

			this.getTotal = function() {
				return totalItems;
			};

		},

		link: function(scope, element, attrs, controller) {
			var input = element.find('input');
			var list = element.find('div').eq(1);
			var listHeight = list[0] ? list[0].getBoundingClientRect().height : 0;
			var init = true;

			scope.$parent.$watch('dialog.index', function(newvalue, oldvalue) {
				if(newvalue === oldvalue) return;
				if(attrs.index === newvalue) {
					input[0].focus();
					attrs.displayed = true;
					//check if set by click on block view
					if(scope.term && scope.term.length && scope.items && scope.items.length)
						scope.query();
				}
				else {
					attrs.displayed = false;
				}
			});

			input.attr('placeholder', attrs.placeholder);

			angular.element(document).bind('keydown', function(e) {

				if(e.target.nodeName !== 'INPUT') return;

				e.stopPropagation();

				if(attrs.displayed === false ) return;

				if (e.keyCode === 9 || e.keyCode === 13 || e.keyCode === 27) {
					e.preventDefault();
				}
				else if (e.keyCode === 40) {
					e.preventDefault();
					scope.$apply(function() { controller.activateNextItem(); });
				}
				else if (e.keyCode === 38) {
					e.preventDefault();
					scope.$apply(function() { controller.activatePreviousItem(); });
				}

			});

			scope.$watchCollection('itemTop', function(values) {
				//based on chosenjs from harvest
				if(values) {
					var visibleTop = $(list).scrollTop();
					var visibleBottom = listHeight + visibleTop;
					var top = values[0];
					var outerHeight = 38;
					var highTop = top + visibleTop;
					var highBottom = highTop + outerHeight;

					if(highBottom >= visibleBottom ) {
						$(list).scrollTop((highBottom - listHeight) > 0 ? (highBottom - listHeight) : 0 );
						$(list).perfectScrollbar('update');
					} else if(highTop < visibleTop) {
						$(list).scrollTop(highTop);
						$(list).perfectScrollbar('update');
					}
				}

			});

			angular.element(document).bind('keyup' , function(e) {
				e.stopPropagation();

				//check if entry point is from input or the mouse is in list item bounds
				if(attrs.displayed === false ) return;

				if (e.keyCode === 9 || e.keyCode === 13) {
					scope.$apply(function() {controller.selectActive()});
				}
			});

			//watch for changes in the truthyness of active property of items
			scope.$watch('items', function(items) {
				if(items && items.length > 0) {

					var items = items.filter(function(item) {
						return item ? item.active === true : true;
					});

					//set total once
					if(init && items.length > 0) {
						init = false;
						controller.setTotal(items.length);
						//check if set by click on block view
						if(scope.term && scope.term.length)
							scope.query();
					}
					else {
						if(controller.getTotal() !== items.length) {
							controller.setFoundItems(items);
						} else {
							controller.setFoundItems(null);
						}
					}

					controller.activate(items.length ? items[0] : null);
				}
			}, true);

			scope.query = function() {
				scope.show({term:scope.term});

				if(scope.term.length === 0)
					controller.reset();

				if(controller.getFoundItems()) {
					controller.activate(controller.getFoundItems()[0]);
				} else if(scope.ltems && scope.items.length > 0){
					controller.activate(scope.items[0]);
				}

				$(list).scrollTop(0);
				$(list).perfectScrollbar('update');
			};
		}
	};
});


genosDirectives.directive('typeaheadItem', function() {
	return {
		require: '^typeahead',
		link: function(scope, element, attr, controller) {
			var item = scope.$eval(attr.typeaheadItem);
			scope.$watch(function() { return controller.isActive(item); }, function(active) {
				if (active) {
					element.removeClass('active');
					element.addClass('active');
					//hack of passing index i to trigger watch collection
					controller.setItemTop($(element).position().top, item.i);
				} else {
					element.removeClass('active');
				}
			});
			element.bind('click', function() {
					scope.$apply(function() {controller.select(item)});
			});
			element.bind('mouseenter', function(e) {
				scope.$apply(function() { controller.activate(item); });
			});
		}
	};
});


//Sigma
genosDirectives.directive('graphLayout', function(safeApply) {
	return  {
		restrict: 'EA',
		replace: true,
		scope: true,
		link: function(scope, element, attrs, controller) {
			var self = this;

			self.scope = scope;

			sigma.classes.graph.addMethod('neighbors', function(nodeId) {
				var k,
					neighbors = {},
					index = this.allNeighborsIndex[nodeId] || {};

				for (k in index)
					neighbors[k] = this.nodesIndex[k];

				return neighbors;
			});

			self.showActive = function(nodeId, node, eventType) {
				var toKeep = self.s.graph.neighbors(nodeId);
				toKeep[nodeId] = node;

				self.s.graph.nodes().forEach(function(n) {
					if (toKeep[n.id]) {
						n.color = n.originalColor;

						if(eventType === 'clickNode')
							n.keep = true;
						else if(eventType === 'emulateOverNode')
							n.emulateHover = true;
						else if(eventType === 'overNode')
							n.hover = true;

					} else {
						n.color = 'transparent';
					}
				});

				self.s.graph.edges().forEach(function(e) {
					if (toKeep[e.source] && toKeep[e.target])
						e.color = e.originalColor;
					else
						e.color = 'transparent';
				});

				self.s.refresh();
			};

			self.reset = function() {
				self.s.graph.nodes().forEach(function(n) {
					n.color = n.originalColor;
					n.keep = false;
					n.hover = false;
					n.emulateHover = false;
				});

				self.s.graph.edges().forEach(function(e) {
						e.color = e.originalColor;
				});

				self.s.refresh();
			};

			self.clickNode = false;

			self.clickedNodeId;

			self.startGraph = function(graph) {
				for(var i = 0, gL = graph.nodes.length; i < gL; i++) {
					graph.nodes[i].keep = false;
					graph.nodes[i].hover = false;
					graph.nodes[i].emulateHover = false;
				}

				if(self.s) {
					self.s.kill();
					$(element).find('canvas').remove();
				}

				//Instantiate sigma:
				self.s = new sigma({
					graph: graph,
					settings: {
						defaultHoverLabelBGColor: 'transparent',
						labelColor: 'node',
						sideMargin: 3,
						labelThreshold: 7
					},
					renderer: {
						container: element[0],
						type: 'canvas'
					}
				});

				self.s.graph.nodes().forEach(function(n) {
					n.originalColor = n.color;
				});

				self.s.graph.edges().forEach(function(e) {
					e.originalColor = e.color;
				});

				self.s.bind('overNode clickNode', function(event) {
					var timeout = function() {
						var eventType = event.type;

						if(eventType === 'clickNode') {
							self.clickNode = true;

							safeApply(self.scope, function() {
								self.scope.graphView.nodeItemId = event.data.node.id;
							});
						}
						else if(self.clickNode && eventType === 'overNode')
							return;

						self.showActive(event.data.node.id, event.data.node, eventType);
					};

					setTimeout(timeout, 10);
				});

				self.s.bind('outNode clickStage', function(event) {
					var timeout = function() {
						var eventType = event.type;

						if((self.clickNode) && eventType === 'outNode') {
							return;

						} else if(self.clickNode && eventType === 'clickStage') {
							//reset selection
							self.clickNode = false;

							safeApply(self.scope, function() {
								self.scope.graphView.nodeItemId = null;
							});
						}

						self.reset();
					};

					setTimeout(timeout, 10);
				});

				document.querySelector('.view-loader').style.visibility = 'hidden';
			};

			self.scope.$watch('graphView.graph', function(graph, oldgraph) {
				if(graph)
					startGraph(graph);
			});

			//listen for active node hovers
			self.scope.$on('nodeActivity', function(event, node, type) {
				if(type === 'emulateOverNode' && !self.clickNode) {
					self.showActive(node.id, node, type);
				}
				else if(type === 'resetNodes'  && !self.clickNode) {
					self.reset();
				}
				else if(type === 'emulateClickNode') {
					if(self.clickedNodeId && self.clickedNodeId === node.id) {
						self.clickedNodeId = null;
						self.clickNode = false;

						self.reset()

						safeApply(self.scope, function() {
							self.scope.graphView.nodeItemId = null;
						});
					}
					else {
						self.clickNode = true;
						self.clickedNodeId = node.id;
						self.showActive(node.id, node, 'clickNode')
					}
				}
			});

			self.scope.$on('zoomIn', function(event) {
				var c = sigma.instances(0).cameras[0];

				sigma.misc.animation.camera(c, {
				  ratio: c.ratio / c.settings('zoomingRatio')
				}, {
				  duration: 200
				});
			});

			self.scope.$on('zoomOut', function(event) {
				var c = sigma.instances(0).cameras[0];

				sigma.misc.animation.camera(c, {
				  ratio: c.ratio * c.settings('zoomingRatio')
				}, {
				  duration: 200
				});
			});

			self.scope.$on('resetView', function(event) {
				var c = sigma.instances(0).cameras[0];

				sigma.misc.animation.camera(c, {
				  ratio: 1
				}, {
				  duration: 200
				});
			});

			self.scope.$on('prettifyView', function(event, value) {
				if(typeof value === 'undefined') return;

				if(value)
					self.s.startForceAtlas2();
				else
					self.s.stopForceAtlas2();
			});
		}
	}
});

genosDirectives.directive('chart', function(collectionService) {
	return {
		restrict: 'EA',
		scope: true,
		link: function(scope, element, attrs, controller) {
			var el = element[0],
				goodCount = 0,
				badCount = 0,
				magObj = {},
				magArr = [],
				magKeys = [],
				totalCount = 0,
				countData = function(mcollection) {
					var collection = mcollection || collectionService.getDefaultCollection();

					totalCount = collection.length; goodCount = 0; badCount = 0; magObj = {};magArr = []; magKeys = [];

					for(var i=0; i < totalCount; i++) {
						var genos = collection[i],
							mag = genos.magnitude,
							repute = genos.repute;

						if(mag && magObj[mag])
							magObj[mag] = ++magObj[mag]
						else if(mag)
							magObj[mag] = 1;

						if(repute) {
							if(repute === 'Good') {
								++goodCount;
							}
							else if(repute === 'Bad') {
								++badCount;
							}
						}
					}
				},
				setDough = function(collection) {
					countData(collection);

					var doughnutData = [
							{
								value : totalCount - (badCount + goodCount),
								label: 'Not Set',
								color : "#CECECE"
							},
							{
								value: goodCount,
								label: 'Good',
								color: "#0CC00C"
							},
							{
								value : badCount,
								label: 'Bad',
								color : "#F55151"
							}            
						],
						myDoughnut = new Chart(el.getContext("2d")).Doughnut(doughnutData, {
							animateRotate : false,					
							/*animationEasing: 'easeInQuad',
							animationSteps: 40,*/
							segmentShowStroke: false
						});
					};        

			scope.$watchCollection(function() {
				return collectionService.getActiveCollection();
			}, 
			function(newValues) {
				if(el.style.display == 'none' || collectionService.getIsSort())  {
					collectionService.setIsSort(false); //reset
					return;
				}

				if(newValues && newValues.length) {
					setDough(newValues); 

					// el.style.visibility = 'visible';
				}
				else {
					// el.style.visibility = 'hidden'; 
			   	}
			});
		}
	};
});

genosDirectives.directive('helpMode', function() {
	return {
		restrict: 'EA',
		link: function(scope, element, attrs, controller) {
			scope.$watch('main.mode.helpMode', function(newvalue, oldvalue) {
				if(newvalue === oldvalue) return;

				var helpPopover = document.querySelector('.help-popover'),
					helpTextEl = angular.element(document.querySelectorAll('[help-text]'));

				helpTextEl.toggleClass('help-mode');

				if(helpTextEl.hasClass('help-mode')) {
					helpTextEl.bind('mouseenter', function() {
						helpPopover.children[1].children[0].innerHTML = this.getAttribute('help-text');
						helpPopover.style.left = this.getBoundingClientRect().left + 'px';
						if(this.hasAttribute('help-top')) {
							helpPopover.style.top = this.getBoundingClientRect().top - this.getBoundingClientRect().height  + 'px';
							angular.element(helpPopover).removeClass('bottom').addClass('top');
						}
						else {
							helpPopover.style.top = this.getBoundingClientRect().top + this.getBoundingClientRect().height + 'px';
							angular.element(helpPopover).removeClass('top').addClass('bottom');
						}
						helpPopover.style.display = 'block';
					});

					helpTextEl.bind('mouseleave', function(event) {
						helpPopover.style.display = 'none';
					});
				}
				else {
					helpTextEl.unbind('mouseenter');
					helpTextEl.unbind('mouseleave');
				}
			});
		}
	};
});

genosDirectives.directive('printMode', function(safeApply) {
	return {
		restrict: 'EA',
		link: function(scope, element, attrs, controller) {
			var match = window.matchMedia("print"),
				clickFlag = false,
				printTableStyle,
				index = 0,
				flag = 0;

			match.addListener(function(m) {
				// function called 3 times !
				if(clickFlag) {
					if (document.querySelector('#no-print')) {
		                printTableStyle = document.querySelector('#no-print');
		                printTableStyle.parentNode.removeChild(printTableStyle);
		            }
					index++;
					if(index === 3) {
						index = 0;
						clickFlag = false;
					}
				}
				else if (scope.main.viewIndex === 0) {
					if (!m.matches) {
						if (!document.querySelector('#no-print')) {
			                printTableStyle = document.createElement("style");
			                printTableStyle.id = 'no-print';
			                printTableStyle.innerHTML = "@media print { .genos-block {display: none} .no-print {display: block !important;}}";
			                document.body.appendChild(printTableStyle);
			            }
						if(!flag) {
							flag = 1;
						}
						else {
							flag = 0;
						}
					}
				}
			});

			element.bind('click', function() {
				clickFlag = true;
				if (scope.main.viewIndex === 0) {
					scope.$broadcast('unCloakAll');
				}
				else {
					window.print();
				}
			});
		}
	};
});

//overlay with dialog
genosDirectives.directive('overlayDialog', function() {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			dialogTitle: '@',
			show: '=',
			ok: '&',
			cancel: '&'
		},
		template: '<div class="overlay">\
					<button class="overlay-close" data-action="overlay-close" ng-click="show = false">×</button>\
					<div class="overlay-dialog">\
						<h3 class="overlay-title">{{dialogTitle}}</h3>\
						<div class="overlay-content clearfix" ng-transclude></div>\
						<div class="overlay-actions">\
							<button class="btn overlay-btn ok-btn" ng-click="ok()">Ok</button>\
							<button class="btn overlay-btn cancel-btn" ng-click="onCancel()">Cancel</button>\
						</div>\
					</div>\
				</div>',
		controller: function($scope) {
			$scope.onCancel = function() {
				$scope.show = false;
				$scope.cancel();
			}
		}
	}
});