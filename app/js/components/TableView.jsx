/**
 * @jsx React.DOM
 */

var React = require('react'),
	TableItemView = require('./TableItemView.jsx');

module.exports = TableView = React.createClass({displayName: 'TableView',
	getInitialState: function() {
		var that = this;

		return {
			rows: (function(that) {
				var rows = [],
					collection = that.props.collection,
					limit = that.props.limit;

				that.pageNumber = 1;

				that.totalItems = that.props.collection.length;

				that.totalPages = Math.round(Math.max(collection.length, limit)/limit);

				for(var i = 0, pL = Math.min((that.pageNumber * limit), collection.length); i < pL; i++) {
				   rows.push(<TableItemView item={collection[i]} counter={i+1} key={"item-"+collection[i].lindex}></TableItemView>);
				}

				return rows;
			}(that)),
			limit: that.props.limit
		};
	},
	loadMore: function(event, limit) {
		var that = this,
			rows = [],
			collection = that.props.collection,
			limit = +limit || that.props.limit;

		that.pageNumber++;

		for(var i = Math.min(that.state.rows.length, (that.pageNumber - 1) * limit), pL = Math.min((that.pageNumber * limit), collection.length); i < pL; i++) {
		   rows.push(<TableItemView item={collection[i]} counter={i+1} key={"item-"+collection[i].lindex}></TableItemView>);
		}

		that.setState({
			rows: that.state.rows.concat(rows),
			limit: limit
		});
	},
	loadAll: function(event) {
		var that = this,
			rows = [],
			collection = that.props.collection;

		that.itemsLoaded = that.state.rows.length;

		that.showLoader();

		that.doLoadAll = true;

		window.setTimeout(function() {
			that.loadMore(null, 500);
		}, 10);
	},
	showLoader: function() {
		var that  = this;		
		that.refs.loader.getDOMNode().style.display = 'block';
		angular.element(that.refs.table.getDOMNode()).addClass('inactive');
	},
	hideLoader: function() {
		var that = this;
		that.refs.loader.getDOMNode().style.display = 'none';
		angular.element(that.refs.table.getDOMNode()).removeClass('inactive');
	},
	componentDidUpdate: function(prevProps, prevState) {
		var that = this;

		if(that.doLoadAll && that.state.rows.length !== prevState.rows.length && that.state.rows.length !== that.totalItems) {
			window.setTimeout(function() {
				that.refs.itemsLoaded.getDOMNode().innerHTML = that.state.rows.length - that.itemsLoaded;
				that.loadMore(null, 500);
			}, 10);
		}
		else {
			if(that.doLoadAll) {
				that.refs.itemsLoaded.getDOMNode().innerHTML = that.state.rows.length - that.itemsLoaded;
			}

			that.hideLoader();
		}
	},
	componentWillMount: function() {
		document.querySelector('.view-loader').style.visibility = 'visible';
	},
	componentDidMount: function() {
		var that = this,
			node = angular.element(that.getDOMNode());

		window.setTimeout(function() {
			document.querySelector('.view-loader').style.visibility = 'hidden';
		}, 100);
	},
	render: function() {
		return <div className="genos-table-container">
					{this.state.rows.length > 0 ? <span className="print-table hint--right" data-hint="The below Table is Print-Friendly">
						<span className="icon-print"></span>
					</span>: null}
					<table ref="table" className="genos-table" ng-cloak cellPadding="0" cellSpacing="0" border="0">
					<colgroup>
						<col></col>
						<col style={{width:'110px'}}></col>
						<col style={{width:'70px'}}></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col style={{width:'250px'}}></col>
						<col></col>
						<col></col>
						<col style={{width:'110px'}}></col>
						<col style={{width:'90px'}}></col>
						<col style={{width:'110px'}}></col>
						<col></col>
						<col></col>
					</colgroup>
					<thead className="genos-table-h">
						<tr>
							<th className="genos-table-hr">
								#
							</th>
							<th className="genos-table-hr">
								Name
							</th>
							<th className="genos-table-hr">
								Repute
							</th>
							<th className="genos-table-hr">
								Mag
							</th>
							<th className="genos-table-hr">
								Freq
							</th>
							<th className="genos-table-hr">
								Ref
							</th>
							<th colSpan={4} className="genos-table-hr">
								Summary
							</th>
							<th className="genos-table-hr">
								Chrom
							</th>
							<th className="genos-table-hr">
								Position
							</th>
							<th className="genos-table-hr">
								MaxMag
							</th>
							<th className="genos-table-hr">
								Gene
							</th>
							<th className="genos-table-hr">
								Mom
							</th>
							<th className="genos-table-hr">
								Dad
							</th>
						</tr>
					</thead> 
					<tbody>{this.state.rows}</tbody>
				</table>
				{this.props.collection.length > this.props.limit && this.state.rows.length !== this.props.collection.length ? <div className="loader-btn-container">
					<button className="load-more-btn" onClick={this.loadMore}>Load More</button>
					<button className="btn show-all-btn" onClick={this.loadAll}>Show All ({this.props.collection.length - this.state.rows.length} Items)</button>
				</div>: null}
				<div className="table-loader-container" ref="loader">
					<div className="loader table-loader"></div>
					<div>
						<span>Loading </span><span className="l-items-loaded" ref="itemsLoaded">{this.state.rows.length}</span><span className="l-of">of <span className="l-total-items">{this.props.collection.length - (this.itemsLoaded || this.state.rows.length)}</span><span>Items</span></span>					
					</div>
				</div>
			</div>
	}
});