/**
 * @jsx React.DOM
 */


var React = require('react');

module.exports = TableItemView = React.createClass({displayName: 'TableItemView',
    render: function() {
        var genos = this.props.item,
            genoTitle = (genos.rsnum ? genos.rsnum : genos.title),
            title = 'http://www.snpedia.com/index.php/' + genoTitle,
            genoGeno = (genos.geno ? genos.geno : null),
            freqColor = genos.freq ? {backgroundColor: genos.freqcolor + ' !important'}: {},
	     shortTitle = (genos.rsnum ? 'http://www.snpedia.com/index.php/' + genoTitle + genoGeno : 'http://www.snpedia.com/index.php/' + genoTitle);



        return (<tr className="genos-table-r">
                    <td className="genos-table-d">
                        {this.props.counter}
                    </td>
                    <td className="genos-table-d">
                        <a className="d-ex-link" target="_blank" href={title}>{genoTitle}</a>
                        <div className="clearfix"></div>
                        {genoGeno ? <a className="d-short-ex-link" target="_blank" href={shortTitle}>{genoGeno}</a> : null}
                    </td>
                    <td className="genos-table-d">
                       {genos.repute ? <span className={'repute '+ genos.repute.toLowerCase()} >{genos.repute}</span>: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.magnitude ? genos.magnitude : ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.freq ? <span className={ genos.freqcolor && genos.freqcolor !== '#FFFFFF' ? 'd-freq-color': ''}  style={genos.freqcolor && genos.freqcolor !== '#FFFFFF' ? freqColor: {}}>{genos.freq}</span>: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.numrefs ? genos.numrefs: ''}
                    </td>
                    <td className="genos-table-d d-summary" colSpan={4}>
                        {genos.genosummary ? genos.genosummary: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.chrom ? genos.chrom : ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.pos ? genos.pos: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.maxmag ? genos.maxmag: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.gene ? genos.gene: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.mom ? genos.mom: ''}
                    </td>
                    <td className="genos-table-d">
                        {genos.dad ? genos.dad: ''}
                    </td>
                </tr>);
    }
});
