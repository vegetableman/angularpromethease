/**
 * @jsx React.DOM
 */

var React = require('react');

module.exports = PaginationView = React.createClass({displayName: 'PaginationView',
	getInitialState: function() {
		this.pageNumber = this.props.pageNumber;

		return {
			totalItems: this.props.totalItems,
			totalPages: this.props.totalPages,
			pageNumber: this.pageNumber
		}
	},
	showPage: function(event) {
		if(event.keyCode === 13) {
			var value = event.target.value;
			if(!isNaN(value) && value > 0 && value <= this.props.totalPages)
				this.props.showPage(this.pageNumber = +value);
		}
	},
	showFirstPage: function() {
		this.props.showFirstPage();
	},
	showLastPage: function() {
		this.props.showLastPage();
	},
	setItemCount: function(count) {
		this.setState({
			totalItems: count
		});
	},
	setPageNumber: function(number) {
		if(this.refs.pageInput && this.pageNumber !== number)
			this.pageNumber = this.refs.pageInput.getDOMNode().value = number;
	},
	getPageNumber: function() {
		return this.pageNumber;
	},
	render: function() {
		return (<div>
					<span className="total-items"><span className="count">{this.state.totalItems}</span>
						{this.state.totalItems > 1 ? <span> items found</span> : <span> item found</span>}
					</span>
					<div className="controls">
						<span className="first-page-wrp hint--top" data-hint="Go to First Page">
							<span className="first-page-btn icon-arrow-circle-up" onClick={this.showFirstPage}></span>
						</span>
						<span className="last-page-wrp hint--top" data-hint="Go to Last Page">
							<span className="last-page-btn icon-arrow-circle-down" onClick={this.showLastPage}></span>
						</span>
						<span className="page-placeholder">Page</span>
						<input type='text' className="page-no-input" ref="pageInput" onKeyUp={this.showPage} defaultValue={this.state.pageNumber}/>
						<span className="total-pages">
							<span>of</span>
							<span className="page-count">{this.state.totalPages}</span>
							{this.state.totalPages > 1 ? <span>Pages</span>: <span>Page</span>}
						</span>
					</div>
				</div>);
	}
});