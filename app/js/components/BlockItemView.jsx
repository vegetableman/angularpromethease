/**
 * @jsx React.DOM
 */

var React = require('react');

module.exports = BlockItemView = React.createClass({displayName: 'BlockItemView',	
	statics: {
	    cloak: function(view) {
	    	if(!view.state.cloaked && view.isMounted()) {
	    		view.setState({cloaked: true});
	    	}
		},
		unCloak: function(view) {
			if(view.state.cloaked && view.isMounted()) {
				view.setState({cloaked: false});
			}
		},
		setBoring: function(view) {
			if(view.isMounted()) {
				view.setState({boring: true});
			}
		},
		hideDetails: function(view) {
			if(view.isMounted() && view.state.showDetails)
				view.setState({showDetails: false});
		},
		showEditMode: function(view, bool) {
			if(view.isMounted())
				view.setState({showEditMode: bool});
		}
	},
	getInitialState: function() {
		return {cloaked: (this.state && this.state.cloaked || false), 
				showMore: (this.state && this.state.showMore || false), 
				showEditOptions: (this.state && this.state.showEditOptions || false),
				boring: (this.state && this.state.boring || false),
				showDetails: (this.state && this.state.showDetails || false),
				showEditMode: (this.state && this.state.showEditMode) || (this.props && this.props.showEditMode) || false,
				top: 0,
				height: 0
				};
	},
	shouldComponentUpdate: function(nextProps, nextState) {
		var cloakState = (this.state.cloaked !== nextState.cloaked),
			showMoreState = (this.state.showMore !== nextState.showMore),
			editMoreState = (this.state.showEditOptions !== nextState.showEditOptions),
			boringState = (this.state.boring !== nextState.boring),
			showEditState = (this.state.showEditMode !== nextState.showEditMode),
			detailsState = (this.state.showDetails !== nextState.showDetails);

		return cloakState || editMoreState || showMoreState || boringState || detailsState || showEditState;
	},
	componentDidMount: function() {
		var el = this.getDOMNode(), that = this, img;

		if(el) {
			var height = el.offsetHeight;

			//cache height to avoid recalculation of height
			// if(!this.props.top) 
				// this.props.top = el.offsetTop;
			this.state.top = this.props.top || el.offsetTop;

			// if(!this.props.height)
				// this.props.height = height;
			this.state.height = this.props.height || height;

			// el.style.minHeight = this.props.height + 'px';

			el.style.minheight = this.state.height + 'px';

			img = this.refs.genosImage ? this.refs.genosImage.getDOMNode(): undefined;

			if(img) {
				img.onload = function() {
					that.props.imgHeight = img.getBoundingClientRect().height + 'px';
					img.parentNode.style.height = that.props.imgHeight;
				};
			}
		}
	},
	componentDidUpdate: function(prevProps, prevState) {
		if(this.state.showMore !== prevState.showMore || this.state.boring !== prevState.boring) {
			var el = this.getDOMNode();

			//reset minheight to get offsetheight
			el.style.minHeight = '';

			var height = el.offsetHeight;
			this.props.height = height;
			el.style.minHeight = height + 'px';
		}
	},
	timeFormat: function(time) {
		return parseInt(time/1000000, 10);
	},
	toggleEditMode: function() {
		this.setState({
			showEditOptions: !this.state.showEditOptions
		});
	},
	getTags: function(tags) {
		var tagArr = [];
		for(var i=0, tL = tags.length; i < tL; i++) {
			tagArr.push(<span className="genos-tag" key={"tag-"+i} onClick={this.setTag}>{tags[i]}</span>)
		}
		return tagArr;
	},
	getClinvarDiseases: function(diseases) {
		var diseases = diseases.split(';'),
			diseasesArr = [];

		for(var i=0, tL = diseases.length; i < tL; i++)
			diseasesArr.push(<span className="clinvar-dtag" key={"clinvar-"+i} onClick={this.setClinvar}>{diseases[i].trim()}</span>);

		return diseasesArr;
	},
	setTag: function(e) {
		this.props.setTag(e.target.innerHTML);
	},
	setClinvar: function(e) {
		this.props.setClinvar(e.target.innerHTML);
	},
	showRsText: function(event) {
		window.setTimeout(function() {
			var el = this.getDOMNode(),
				height = el.offsetHeight;

			el.style.minHeight =  height + 'px';
			this.props.height = height;
			this.setState({
				showMore: !this.state.showMore
			});
		}.bind(this), 10);
	},
	showDetails: function(e) {
		var el = angular.element(this.refs.info.getDOMNode()),
			target = e.target;

		if(BlockItemView.previousView)
			BlockItemView.previousView.setState({
				showDetails: false
			});

		this.infoPos = (target.offsetLeft) + 'px';
		this.setState({
			showDetails: !this.state.showDetails
		});

		BlockItemView.previousView = this;
	},
	showBoring: function() {
		this.props.showBoring(this.props.item.lindex);
	},
	render: function() {
		var genos = this.props.item,
			cloaked = this.state.cloaked,
			boring = this.state.boring,
			showMoreState = this.state.showMore,
			showEditMode = this.state.showEditMode ? {display: "block"} : {display: "none"},
			showEditOptions = this.state.showEditOptions,
			detailsState = this.state.showDetails,
			genoTitle = (genos.rsnum ? genos.rsnum : genos.title),
			title = 'http://www.snpedia.com/index.php/' + genoTitle,
			genoGeno = (genos.geno ? genos.geno : null),
			editLink = (genos.rsnum ? 'http://www.snpedia.com/index.php/Special:FormEdit/Genotype/'+ genoTitle + genoGeno : 'http://www.snpedia.com/index.php/Special:FormEdit/Genoset/'+ genoTitle),
			shortTitle = (genos.rsnum ? 'http://www.snpedia.com/index.php/' + genoTitle + genoGeno : 'http://www.snpedia.com/index.php/' + genoTitle);

		if(boring) {
			genos.repute = 'Good';
			genos.magnitude = 0;
		}

	  	if(cloaked) {
	  		return <div className="genos-item" style={{minHeight: this.state.height + 'px'}}>
	  					<div className="genos-item-header header">
							<div className="genos-summ">
						 		{genos.genosummary}
					   		</div>
						    <div className="genos-title">
								<a className="ex-link" target="_blank" href={title}>{genoTitle}</a>
								{genoGeno ? <a className="ex-link short" target="_blank" href={shortTitle}>{genoGeno}</a> : null}
					   		</div>
					   </div>
					   <div className="genos-loading">
				   			Loading....
				   		</div>
	  				</div>
	  	}

		var freqColor = genos.freq ? {backgroundColor: genos.freqcolor}: {};
		var infoPos = this.infoPos ? {left: this.infoPos}: {};
		var imgHeight = this.props.imgHeight ? {height: this.props.imgHeight} : {};

		return (
		<div className="genos-item" key={this.props.key}>
			<div className="genos-item-header header">
				{genos.genosummary ? <div className="genos-summ">
			 		{genos.genosummary}
		   		</div>: null}
			   <div className="genos-title">
					<a className="ex-link" target="_blank" href={title}>{genoTitle}</a>
					{genoGeno ? <a className="ex-link short" target="_blank" href={shortTitle}>{genoGeno}</a> : null}
					<span className={'show-details '+ (genos.repute ? genos.repute.toLowerCase(): '') + (detailsState? ' active': '')} onClick={this.showDetails}>Show Details</span>
			   </div>
		   </div>
		   <div className="genos-details">
			   <div className={detailsState ? "genos-info active": "genos-info"} ref="info" style={infoPos}>
					<div className="genos-info-b">
				 		<div className="genos-info-r">
						   {genos.repute ? <div className="genos-info-c" data-title="Repute">
						   		<span className={'repute '+ genos.repute.toLowerCase()} >{genos.repute}</span>
						   </div>: null}
						   {genos.magnitude ? <div className="genos-info-c" data-title="Magnitude">
								{genos.magnitude}
						   </div>: null}
						   {genos.orientation ? <div className="genos-info-c" data-title="Orientation">
							   {genos.orientation}
						   </div> : null}
						   {genos.freq ? <div className="genos-info-c" data-title="Frequency">
			               		<span className={ genos.freqcolor && genos.freqcolor !== '#FFFFFF' ? 'freq-color': ''}  style={freqColor}>{genos.freq}</span>
			               </div>: null}
			               {genos.gmaf ? <div className="genos-info-c" data-title="GMAF">
			               		{genos.gmaf}
			               </div>: null}
			               {genos.numrefs ? <div className="genos-info-c" data-title="References">
				               {genos.numrefs}
			               </div>: null}
			               {genos.gene ? <div className="genos-info-c" data-title="Gene">
				               {genos.gene}
			               </div>: null}
			               {genos.chrom ? <div className="genos-info-c" data-title="Chromosome">
			               	{genos.chrom}
			               </div>: null}
			               {genos.pos ? <div className="genos-info-c" data-title="Position">
			               	{genos.pos}
			               </div>: null}
			               {genos.maxmag ? <div className="genos-info-c" data-title="Max Magnitude">
			               	{genos.maxmag}
			               </div>: null}
			               {genos.rstime ? <div className="genos-info-c" data-title="Rs Time">
			               	{this.timeFormat(genos.rstime)}
			               </div>: null}
			               {genos.genotime ? <div className="genos-info-c" data-title="Geno Time">
			               	{this.timeFormat(genos.genotime)}
			               </div>: null}
			               {genos.mom ? <div className="genos-info-c" data-title="Mom">
			               {genos.mom}
			               </div>: null}
			               {genos.dad ? <div className="genos-info-c" data-title="Dad">
			               	{genos.dad}
			               </div>: null}
						</div>
					</div>
				</div>
				<div className="genos-ref">
					<div className="genos-body">
						 <p ref="genoBody">
						 	{genos.imgbase64 && genos.imgbase64 !== null ? <div className="genos-img" style={imgHeight}>
							  <img className="genos-image" src={genos.imgbase64} ref="genosImage"/>
							 </div>: null}
							{genos.imgurl && genos.imgurl !== null ? <div className="genos-img" style={imgHeight}>
							  <img className="genos-image" src={genos.imgurl} ref="genosImage"/>
							 </div>: null}
						 	{genos.genobody ? <p>{genos.genobody}</p> : null}
						 </p>
						 {genos.rstext && genos.genobody ? <div className={showMoreState ? "show-more hide" : "show-more"} onClick={this.showRsText} >..more</div> : null}
						 {genos.rstext ? <div ref="rsText" className={(!genos.genobody || showMoreState)? "genos-text active": "genos-text"}>{genos.rstext}</div> : null}
					</div>
	           </div>
	           {genos.clinvar_1 ? <div className="genos-clinvar-tags">
		             <span className="hint--top dna" data-hint="ClinVar Diseases"><span className="icon-dna"></span></span>
		             {genos.clinvar_1 === "1" ? <span className="clinvar-itag untested"><span className="icon-info"></span>Untested</span> : null}
		             {genos.clinvar_1 === "2" ? <span className="clinvar-itag n-patho"><span className="icon-info"></span>Non-pathogenic</span>: null}
		             {genos.clinvar_1 === "3" ? <span className="clinvar-itag pn-patho"><span className="icon-info"></span>Probable-non-pathogenic</span>: null}
		             {genos.clinvar_1 === "4" ? <span className="clinvar-itag p-patho"><span className="icon-info"></span>Probable-pathogenic</span> : null}
		             {genos.clinvar_1 === "5" ? <span className="clinvar-itag patho"><span className="icon-info"></span>Pathogenic</span> : null}
		             {genos.clinvar_1 === "6" ? <span className="clinvar-itag drug-r"><span className="icon-info"></span>Drug-response</span>: null}
		             {genos.clinvar_1 === "7" ? <span className="clinvar-itag histocmp"><span className="icon-info"></span>Histocompatibility</span>: null}
		             {genos.clinvar_1 === "255" ? <span className="clinvar-itag oth"><span className="icon-info"></span>Other</span>: null}
		             {genos.clinvar_1 === "" ? <span className="clinvar-itag def">??</span>: null}
		             {genos.clinvar_disease && genos.clinvar_disease.length > 0 ? <span>{this.getClinvarDiseases(genos.clinvar_disease)}</span>: null}
				</div>: null}
				{genos.tags && genos.tags.length > 0 ? <div className="genos-tags">
		            <span className="hint--top branch" data-hint="Tags"><span className="icon-flow-branch"></span></span>
		            {this.getTags(genos.tags)}
            	</div>: null}
			</div>
			 <div className="genos-edit-container" style={showEditMode}>
				<a className="genos-edit-mode hint--right" data-hint="Edit Mode" onClick={this.toggleEditMode}>
				 <span className="icon-pencil"></span>
				</a>
				<div className={showEditOptions ? "genos-edit active": "genos-edit"} ref="editMode">
					<a onClick={this.showBoring}>Set Boring</a>
					<a className="ex-link" target="_blank" href={editLink}>Edit</a>
				</div>
           </div>
		</div>);
	}
});