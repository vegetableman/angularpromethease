/**
 * @jsx React.DOM
 */

var React = require('react'),
	BlockItemView = require('./BlockItemView.jsx'),
    PaginationView = React.createFactory(require('./PaginationView.jsx'));

module.exports = BlockView = React.createClass({displayName: 'BlockView',
    debounce: function(func, wait) {
        var timeout;

        return function() {
            var later = function() {
                timeout = null;
                func();
            };
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    },

    findTopView: function(childViews, viewportTop, min, max) {
        if (max < min) { return min; }

        var mid = Math.floor((min + max) / 2),
                view = childViews[mid],
                viewBottom = (this.biDirectional ? view.getDOMNode().offsetTop : view.state.top) + view.state.height;

        if (viewBottom > viewportTop) {
            return this.findTopView(childViews, viewportTop, min, mid-1);
        } else {
            return this.findTopView(childViews, viewportTop, mid+1, max);
        }
    },

    cloaker: function(childViews) {

        if (!childViews || !childViews.length) return;

        var that = this,
            windowTop = that.windowTop = (that.w.scrollY || that.w.document.documentElement.scrollTop),
            viewportTop = windowTop,
            windowHeight = that.windowHeight,
            windowBottom = windowTop + windowHeight,
            viewportBottom = windowBottom + windowHeight,
            topView = that.findTopView(childViews, viewportTop, 0, childViews.length-1),
            onScreen = [],
            toUnCloak = [],
            toCloak = [];

        var bottomView = topView;

         // Find what's the bottomView
        while (bottomView < childViews.length) {
            var view = childViews[bottomView],
                viewTop = that.biDirectional ? view.getDOMNode().offsetTop: view.state.top,
                viewBottom = viewTop + view.state.height;

            //break on reaching the bottomView
            if (viewTop > viewportBottom)
                break;

            toUnCloak.push(childViews[bottomView]);

            //onscreen
            if (viewBottom > windowTop && viewTop <= windowBottom) {
              onScreen.push(view);
            }

            bottomView++;
        }

        if (bottomView >= childViews.length) {
            bottomView = childViews.length - 1;
        }

        //Get views above and below the viewport.
        toCloak = childViews.slice(0, topView).concat(childViews.slice(bottomView+1, childViews.length));

        for (var j=0, tUCL = toUnCloak.length; j < tUCL; j++) {
            var view = toUnCloak[j];
            if (view)
                BlockItemView.unCloak(view);
        }

        for (var i=0, tCL = toCloak.length; i < tCL; i++) {
            var view = toCloak[i];
            if (view && !view.state.cloaked)
                BlockItemView.cloak(view);
        }

        return bottomView;

    },

    loadMore: function(rows, limit, size, totalPages) {
        var childViews,
            that = this,
            bottomView = that.cloaker(childViews = rows),
            currPageNumber = that.paginateView.getPageNumber();

        //Set page number on pagination view.
        if (!!childViews[bottomView-1]) {
            currPageNumber = childViews[bottomView-1].props.pageNumber
            that.paginateView.setPageNumber(currPageNumber);
        }

        //If count less than limit or last page, hide the loader.
        if (limit >= size || (totalPages === currPageNumber && bottomView === childViews.length - 1)) {
            that.props.loaderBottom.style.display = 'none';
            return;
        }

        //If not first page and bidirectional
        if (that.biDirectional && currPageNumber !== 1) {
            //If on top of current page
            if (that.windowTop <= childViews[0].getDOMNode().offsetTop && that.topScrollFlag) {
                that.prevScrollHeight = document.body.scrollHeight;
                clearTimeout(that.timeout);
                that.timeout = window.setTimeout(function() {
                    that.renderRows('top', currPageNumber);
                }, 150);

                if (that.showScrollInfo) {
                    that.showScrollInfo = false;
                    angular.element(that.scrollInfo).removeClass('active');
                }
            }

            that.hideLoaderTopFlag = false;
        }

        //If on first page
        else if (currPageNumber === 1) {
            //If top loader visible, hide it.
            if (!that.hideLoaderTopFlag) {
                that.hideLoaderTopFlag = true;
                that.props.loaderTop.style.display = 'none';
            }
        }

        //If on the bottom of the page.
        if (bottomView === childViews.length - 1) {
            var view = childViews[bottomView];
            //Check if really on the bottom of page and no other pages after the current page.
            if (that.pageArr.indexOf(currPageNumber + 1) < 0 &&
                that.windowTop + that.windowHeight > view.state.top + view.state.height) {
                    clearTimeout(that.timeout);
                    that.timeout = window.setTimeout(function() {
                        that.renderRows('bottom', currPageNumber);
                    }, 150);
            }
        }

        that.topScrollFlag = true;
    },

    renderRows: function(morientation, pageNumber) {
        var that = this,
            rows = [],
            isBottom = morientation === 'bottom' ? 1: 0,
            collection = that.props.collection,
            limit = that.props.limit;

        that.pageNumber = pageNumber || that.pageNumber;

        if (!!isBottom)
            that.pageNumber++;
        else
            that.pageNumber--;

        //new rows
        for (var i = Math.max(0, (that.pageNumber - 1) * limit), pL = Math.min((that.pageNumber * limit), collection.length); i < pL; i++) {
           rows.push({item: collection[i], pageNumber: that.pageNumber});
        }

        if (that.pageArr.indexOf(that.pageNumber) < 0)
            that.pageArr.push(that.pageNumber);

        if (that.isMounted()) {
            //if bottom
            if (!!isBottom) {
                that.setState({
                    rows: that.state.rows.concat(rows),
                    limit: limit
                });
            }
            //if top
            else {
                rows = rows.concat(that.state.rows);
                if (that.isMounted()) {
                    that.setState({
                        rows: rows,
                        limit: limit
                    });
                }
            }
        }
    },

    setTag: function(tag) {
        this.props.setTag(tag);
    },

    setClinvar: function(clinvar) {
        this.props.setClinvar(clinvar);
    },

    showBoring: function(index) {
        this.props.showBoring(index);
    },

	getInitialState: function() {
		var that = this;

        that.pageArr = [];
        that.topScrollFlag = true;
        that.scrollInfo = that.props.scrollInfo;
        that.onResizeProp = that.props.onResize;
        that.showScrollInfo = false;
        that.lastPage = that.props.lastPage;
        that.pageInputValue = that.props.pageInputValue;
        that.limit = that.props.limit;
        that.itemCount = that.props.collection.length;
        that.paginationContainer = that.props.paginationContainer;
        that.totalPages = Math.round(Math.max(that.itemCount, that.limit)/that.limit);

	    return {
	    	rows: (function(that) {
	    		var rows = [],
                    collection = that.props.collection,
                    limit = that.props.limit;

                if (that.pageInputValue)
                    that.pageNumber = that.pageInputValue;
                else if (that.lastPage)
                    that.pageNumber = Math.round(that.itemCount / limit);
                else
                    that.pageNumber = 1;

                that.biDirectional = that.pageNumber > 1;
                that.firstPage = that.pageNumber;
                that.pageArr.push(that.pageNumber);

                for (var i = Math.max(0, (that.pageNumber - 1) * limit), pL = Math.min((that.pageNumber * limit), collection.length); i < pL; i++) {
                   rows.push({item: collection[i], pageNumber: that.pageNumber});
                }

				return rows;
	    	}(that)),
	    	limit: that.props.limit
	    };
	},

    shouldComponentUpdate: function(nextProps, nextState) {
        var propsChanged = this.props !== nextProps;
        var stateChanged = this.state !== nextState;
        return propsChanged || stateChanged;
    },

    componentDidUpdate: function() {
        var that  = this,
            children = that.refs,
            rows = [],
            comparator = function(a, b) {
                if(a.props.item.lindex < b.props.item.lindex) return -1;
                if(a.props.item.lindex > b.props.item.lindex) return 1;
                return 0;
            };

        Object.keys(children).forEach(function(key) {
            rows.push(children[key]);
        });

        //Added ascending sorting to fix cloaking issues with top scrolling.
        that.rows = rows.sort(comparator);

        //Scroll back to last item of a page in case of scrolling up.
        if (that.biDirectional && that.windowTop <= that.rows[0].getDOMNode().offsetTop) {
            window.scrollTo(0, document.body.scrollHeight - that.prevScrollHeight); //cross-browser compatible
        }
    },

    componentWillUnmount: function() {
        document.querySelector('.view-loader').style.visibility = 'visible';
    },

	componentDidMount: function() {
        var that = this,
            showPage = function(value) {
                var rows = that.rows,
                    index = that.pageArr.indexOf(value),
                    rerender = index < 0;

                that.props.showPage(rerender, !rerender ? rows[rows.length - (that.limit * (index + 1))].getDOMNode().offsetTop : 0, value);
            },
            showFirstPage = function() {
                that.props.showFirstPage(that.pageArr.indexOf(1) < 0);
            },
            showLastPage = function() {
                that.props.showLastPage(that.pageArr.indexOf(that.totalPages) < 0);
            };

        that.w = window;

        //DOM handling --
        document.querySelector('.view-loader').style.visibility = 'hidden';
        window.setTimeout(function() {
            if (this.isMounted())
                angular.element(this.getDOMNode()).removeClass('inactive');
        }.bind(that), 100);

        //Store rows --
        that.rows = Object.keys(that.refs).map(function(key) {
            return that.refs[key];
        });

        //Handle Events --
        that.attachScroll();
        // that.attachResize();
        that.toggleLoader();
        that.subscribeToScopeEvents();

        //Handle pagination and it's related methods --
        angular.element(this.paginationContainer).removeClass('inactive');
        React.unmountComponentAtNode(that.paginationContainer);
        that.paginateView = React.render(PaginationView({
                                    showPage: showPage,
                                    totalItems:that.itemCount,
                                    totalPages: that.totalPages,
                                    pageNumber: that.pageNumber,
                                    showFirstPage: showFirstPage,
                                    showLastPage: showLastPage,
                                }), that.paginationContainer);


        document.addEventListener('click', function(e) {
            if (!$(e.target).closest('.show-details').length
                && !$(e.target).closest('.genos-edit-mode').length
                && !$(e.target).closest('.genos-edit').length
                && !$(e.target).closest('.overlay').length
                && !$(e.target).closest('.genos-info').length)
                for (var i=0, rows = that.rows, rL = rows.length; i < rL; i++) {
                    BlockItemView.hideDetails(rows[i]);
                }
        });
	},

    attachScroll: function() {
        var that = this,
            scrollOffset = 400;

        //@later way too slow
        //that.debounceInterval = window.innerWidth > 600 ? debounceInterval : debounceInterval * 3;
        that.onScroll = that.debounce(function() {
            that.loadMore(that.rows, that.state.limit, that.itemCount, that.totalPages);
        }, 10);

        that.windowHeight = that.w.innerHeight;
        that.w.addEventListener('scroll', that.onScroll);
    },

    attachResize: function() {
        var that = this;

        that.onResize = that.debounce(function() {
            that.onResizeProp(that.state.rows.length)
        }, 250);

        that.w.addEventListener('resize', that.onResize);
    },

    detachEvents: function() {
        //imp to remove to avoid multiple event handlers
        this.w.removeEventListener('scroll', this.onScroll);
        this.w.removeEventListener('resize', this.onResize);
    },

    subscribeToScopeEvents: function() {
        var that = this,
            node = angular.element(that.getDOMNode());

        node.scope().$on('disableBlockView', function() {
            that.detachEvents();
        });

        node.scope().$on('enableBlockView', function() {
            that.attachScroll();
            if (that.isMounted())
                angular.element(that.getDOMNode()).scope().$emit('hideViewLoader');
        });

        node.scope().$on('unCloakAll', function() {
            var rows = that.rows || [];

            document.querySelector('.view-loader').style.visibility = 'visible';

            for (var i=0, tCL = rows.length; i < tCL; i++) {
                var view = rows[i];
                if (view && view.state.cloaked) {
                    BlockItemView.unCloak(view);
                }
            }

            //Check if view is mounted and not cloaked.
            function check() {
                var AllUnCloaked = rows.every(function(row) {
                    return !row.state.cloaked && row._lifeCycleState === "MOUNTED";
                });

                if (AllUnCloaked) {
                    document.querySelector('.view-loader').style.visibility = 'hidden';
                    window.print();
                }
                else {
                    window.setTimeout(check, 1000);
                }
            }

            check();
        });

        node.scope().$on('editorMode', function(event, value) {
            var rows = that.rows || [];
            that.showEditMode = value;

            for (var i=0, tCL = rows.length; i < tCL; i++) {
                var view = rows[i];
                if (view) {
                    BlockItemView.showEditMode(view, value);
                }
            }
        });

        node.scope().$on('boringUpdate', function(event, index) {
            var rows = that.rows;
            for (var i = 0, rL = rows.length; i < rL; i++) {
                if (rows[i].props.item.lindex === index) {
                    BlockItemView.setBoring(rows[i]);
                    break;
                }
            }
        });
    },

    toggleLoader: function() {
        var that = this,
            props = this.props;

        window.setTimeout(function() {
            //If first page
            if (that.pageNumber ===  1 && that.totalPages > 1) {
                props.loaderTop.style.display = 'none';
                props.loaderBottom.style.display = 'block';
            }

            //Last page and not bidirectional
            else if (that.pageNumber === that.totalPages && that.totalPages > 1) {
                props.loaderTop.style.display = 'block';
            }

            //If limit reached.
            else if (props.limit > that.state.rows.length) {
                props.loaderBottom.style.display = 'none';
            }

            // If bidirectional
            else {
                if (that.biDirectional) {
                    //If not last page, hide loaderTop
                    if (that.totalPages > 1 && that.pageNumber !== that.totalPages) {
                        props.loaderTop.style.display = 'none';
                        that.topScrollFlag = false;
                        angular.element(that.scrollInfo).addClass('active');
                    } else {
                        props.loaderTop.style.display = 'block';
                        angular.element(that.scrollInfo).removeClass('active');
                    }
                    that.showScrollInfo = true;
                }
                props.loaderBottom.style.display = 'block';
            }
        }, 1);
    },

    componentWillUnmount: function() {
        this.detachEvents();
        this.lastPage = this.pageInputValue = null;
        this.props.loaderTop.style.display = 'none';
        this.props.loaderBottom.style.display = 'none';
        this.paginateView.setItemCount(0);

        angular.element(this.scrollInfo).removeClass('active');
        angular.element(this.paginationContainer).addClass('inactive');

        BlockItemView.previousView = null;
    },

	render: function() {
        var that = this,
            rows = [];

        //Using lindex as part of ref
        that.state.rows.forEach(function(row) {
            rows.push(<BlockItemView ref={"blockItemView_"+row.item.lindex} item={row.item} pageNumber={row.pageNumber} showBoring={that.showBoring} setTag={that.setTag} setClinvar={that.setClinvar} key={"item-"+row.item.lindex} showEditMode={that.showEditMode}></BlockItemView>);
        });

		return <div className="clearfix inactive">{rows}</div>;
	}
});