(function () {
  //require angular and dependencies
  require('angular/angular');
  require('../../vendor_libs/angular-touch/angular-touch.js');
  require('../../vendor_libs/venturocket-angular-slider/build/angular-slider.js');
  require('../../vendor_libs/angular-perfect-scrollbar/dependencies/perfect-scrollbar.js');
  require('../../vendor_libs/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js');
  require('../../vendor_libs/position.js');
  require('../../vendor_libs/tooltip.js');
  require('../../vendor_libs/popover.js');
  require('../../vendor_libs/bindhtmlunsafe.js');
  require('../../vendor_libs/headroom.min.js');
  require('../../vendor_libs/angular.headroom.min.js');
  //require app components
  require('./directives/GenosDirectives');
  require('./services/GenosDB');
  require('./services/GenosSort');
  require('./services/GenosFilters');
  require('./services/GenosCollection');
  require('./services/GenosProviders');
  //require controllers
  var genosController = require('./controllers/GenosController'), genosGraphController = require('./controllers/GenosGraphController');
  //declare core module
  angular.module('promethease', [
    'GenosDirectives',
    'GenosDB',
    'GenosFilters',
    'GenosCollection',
    'GenosSort',
    'GenosProviders',
    'vr.directives.slider',
    'perfect_scrollbar',
    'ui.bootstrap.tooltip',
    'ui.bootstrap.popover',
    'headroom'
  ]).controller('GenosController', genosController.GenosController).controller('GenosGraphController', genosGraphController.GenosGraphController);
}());