var crossFilterSort = require('../../../vendor_libs/crossfilter-sort');

var GenosCollection = function(db, sorter, filterService) {
	this.db = db;
	this.sorter = sorter;
	this.filterService = filterService;

	this.defaultCollection = [];
    this.defaultSearchCollection = [];
	this.collection = []; //mutable
    this.searchCollection = [];
    this.tagCollection = [];
    this.clinvarCollection = [];

    this.isFilterSetup = false;
    this.isSearchResult = false;
    this.newRun = false;
    this.limit = 10;

    this.isSort = false;

    //counts
    this.badReputeCount = 0;
    this.goodReputeCount = 0;
};

GenosCollection.prototype.getLimit = function() {
	return this.limit;
};

GenosCollection.prototype.setUpCollection = function() {
    var self = this,
    	collection = window.mygenos,
        rsObj = window.rsinfo,
    	boringIndexes = self.db.getLocalItem('boringIndexes'),
        goodRepute = 0, badRepute = 0,
        isASNP = function(genos) {
            if (typeof(genos['rsnum']) !== 'undefined' && genos['rsnum']) return 1;
            if (typeof(genos['title']) !== 'undefined' && genos['title']) return 0;
            if (typeof(genos['23andMe SNP']) !== 'undefined' && genos['23andMe SNP']) return 1;
            return 0;
        };

    //clean up globals
    window.mygenos = null;
    window.rsinfo = null;

    for(var i=0, gcL = collection.length; i < gcL; i++) {
        collection[i].lindex = i;

        if(boringIndexes && boringIndexes.indexOf(i) >= 0) {
            collection[i].repute = 'Good';
            collection[i].magnitude = 0;
        }

        var genos = collection[i];

        if (isASNP(genos)) {
            if (rsObj[genos.rsnum]) {
            var rsinfo = rsObj[genos.rsnum],
                magArr = [];

            for(var j in rsinfo) {
              if(rsinfo.hasOwnProperty(j))
                magArr.push(rsinfo[j].mag);
            }
            if(magArr.length)
              collection[i].maxmag = Math.max.apply(Math, magArr);
            }
        }
    }

	this.setCollection(collection);
	this.setDefaultCollection(collection.slice(0));
	this.setUpCollectionIndex();
},

GenosCollection.prototype.setUpCollectionIndex = function() {
	var self = this,
        collection = self.getCollection(),
        defaultCollection = self.getDefaultCollection();

    var median = Math.round(defaultCollection.length/2),
        last = defaultCollection.length - 1,
        uniqueIdx = (defaultCollection[0].title ? defaultCollection[0].title : defaultCollection[0].rsnum) + '|'
	                + defaultCollection.length + '|'
                    + (defaultCollection[median].title ?
                        defaultCollection[median].title : defaultCollection[median].rsnum) + '|'
	                + (defaultCollection[last].title ?
                        defaultCollection[last].title : defaultCollection[last].rsnum)+ '|'
	                + window.location.href;

    if(!self.db.isStored('indexed', 'maxMagnitude', 'minMagnitude', 'minMagnitude', 'minRef', 'maxRef')
    		|| self.db.getLocalItem('indexed') !== uniqueIdx) {
        var tcounter = 0,
            ccounter = 0,
	        tagMap = {},
	        tagCollection = [],
	        clinvarCollection = [],
	        clinvarMap = {},
	        maxMagnitude = Number.NEGATIVE_INFINITY,
	        minMagnitude = Number.POSITIVE_INFINITY,
	        maxRef = Number.NEGATIVE_INFINITY,
	        minRef = Number.POSITIVE_INFINITY;

        for(var i=0, count = collection.length; i<count; i++) {
			//store tag occurences count and object indexes
            var genos = collection[i];
                tags = genos.tags,
                tagCount =  tags ? tags.length : 0,
                j = 0;

             while(j < tagCount) {
                var tag = tags[j];

                if(tagMap[tag]) {
                    var tagObj = tagMap[tag],
                        vArr = [];

                    tagObj.c = parseInt(tagObj.c) + 1;

                    for(var k=0, len = tagObj.v.length; k<len; k++) {
                        vArr.push(tagObj.v[k]);
                    }

                    vArr.push(i);
                    tagObj.v = vArr;
                }
                else {
                   var tmptagObj = {i: tcounter++, key: tag, c: 1, v: [i]};
                    tagMap[tag] = tmptagObj;
                    tagCollection.push(tmptagObj);
                }
                j++;
            }

            if(tags)
                tcounter++;

            //stores clinvar occurences and count
            if(genos.clinvar_disease) {
                var clinvarkeyArr = genos.clinvar_disease.split(/;\s*/);

                while(clinvarkeyArr.length > 0) {
                  var clinvarItem = clinvarkeyArr.shift();

                  if(clinvarMap[clinvarItem]) {
                    var clinvarObj = clinvarMap[clinvarItem],
                        vArr = [];

                    clinvarObj.c = parseInt(clinvarObj.c) + 1;

                    for(var k=0, len = clinvarObj.v.length; k<len; k++) {
                      vArr.push(clinvarObj.v[k]);
                    }
                    vArr.push(i);

                    clinvarObj.v = vArr;
                  }
                  else {
                    var tmpClinvarObj = {i: ccounter++, key: clinvarItem, c: 1, v: [i]};
                    clinvarMap[clinvarItem] = tmpClinvarObj;
                    clinvarCollection.push(tmpClinvarObj);
                  }

                }
                ccounter++;
            }

            //store max and min magnitude
            var magnitude = 1;
            if(genos.magnitude)
                magnitude = parseFloat(genos.magnitude);

            maxMagnitude = Math.max(maxMagnitude,magnitude);
            minMagnitude = Math.min(minMagnitude,magnitude);

            //store min and max refs
            if(genos.numrefs) {
                var numRefs = parseInt(genos.numrefs);
                maxRef = Math.max(maxRef,numRefs);
                minRef = Math.min(minRef,numRefs);
            }
        }


        self.filterService.setClinvarCollection(crossFilterSort.sort(clinvarCollection).dimension(function(d) {
            return d['key'];
        }).bottom(Infinity));
        self.filterService.setTagCollection(crossFilterSort.sort(tagCollection).dimension(function(d) {
            return d['key'];
        }).bottom(Infinity), uniqueIdx);
    	self.filterService.setMaxMagnitude(maxMagnitude);
    	self.filterService.setMinMagnitude(minMagnitude);
    	self.filterService.setMaxRef(maxRef);
    	self.filterService.setMinRef(minRef);
	}
};

GenosCollection.prototype.sortCollection = function(type, reverse) {
    if(this.getIsSearchResult()) {
        this.setSearchCollection(this.sorter.sort(this.getSearchCollection(), type, reverse));
    }
    else {
        this.setCollection(this.sorter.sort(this.getCollection(), type, reverse));
    }
};

GenosCollection.prototype.filterCollection = function(filters) {
    if(this.getIsSearchResult())
        this.setSearchCollection(this.sorter.sort(this.filterService.filter(this.getDefaultSearchCollection(), filters)));
    else
        this.setCollection(this.sorter.sort(this.filterService.filter(this.getDefaultCollection(), filters)));
};

GenosCollection.prototype.getDefaultCollection = function() {
	return this.defaultCollection;
};

GenosCollection.prototype.setDefaultCollection = function(mcollection) {
    return this.defaultCollection = mcollection;
};

GenosCollection.prototype.getDefaultSearchCollection = function() {
    return this.defaultSearchCollection;
};

GenosCollection.prototype.setDefaultSearchCollection = function(mcollection) {
    return this.defaultSearchCollection = mcollection;
};

GenosCollection.prototype.getCollection = function() {
	return this.collection;
};

GenosCollection.prototype.setCollection = function(mcollection) {
    this.collection = mcollection;
};

GenosCollection.prototype.setFilteredCollection = function() {
    this.setCollection(this.sorter.sort(this.filterService.filter(this.getDefaultCollection(), null)));
    this.setIsSearchResult(false);
};

GenosCollection.prototype.setIsSearchResult = function(misSearchResult) {
	this.isSearchResult = misSearchResult
};

GenosCollection.prototype.getIsSearchResult = function() {
	return this.isSearchResult;
};

GenosCollection.prototype.setSearchCollection = function(msearchCollection, setDefault) {
    if(setDefault)
        this.setDefaultSearchCollection(msearchCollection.slice(0));
	this.searchCollection = this.sorter.sort(this.filterService.filter(msearchCollection));
    this.setIsSearchResult(true);
};

GenosCollection.prototype.getSearchCollection = function() {
	return this.searchCollection;
};

GenosCollection.prototype.getActiveCollection = function() {
    return this.getIsSearchResult() ? this.getSearchCollection() : this.getCollection();
};

GenosCollection.prototype.setIsSort = function(isSort) {
    this.isSort = isSort;
};

GenosCollection.prototype.getIsSort = function(isSort) {
    return this.isSort;
};

GenosCollection.prototype.setBoring = function(index) {
	var self = this;

	if(self.db.isStored('boringIndexes')) {
        var arr = JSON.parse(self.db.getLocalItem('boringIndexes'));
        if(arr.indexOf(index) < 0) {
            arr.push(index);
            self.db.setLocalItem('boringIndexes', JSON.stringify(arr));
         }
    } else {
        self.db.setLocalItem('boringIndexes', JSON.stringify([index]));
    }
};

module.exports = angular.module('GenosCollection',[]).factory('collectionService', function(db, sorter, filterService) {
	return new GenosCollection(db, sorter, filterService);
});