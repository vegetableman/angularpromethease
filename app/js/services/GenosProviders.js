//Utils for Promethease
//@vegetableman

var genosProviders;

module.exports = genosProviders = angular.module('GenosProviders', ['GenosDB']);

genosProviders.provider('debounce', function() {
    this.$get = function() {
        return function (func, wait) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    func.apply(context, args);
                };
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
            };
        };
    }
});


//sniff is mobile
genosProviders.provider('isMobile', function() {
    this.$get = function() {
        return function(func) {
            return func(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
        };
    };
});

//based on headroom.js
genosProviders.provider('rAFDebounce', function() {
    this.$get = function() {
        return function(func) {
            var Debouncer = function (callback) {
                this.callback = callback;
                this.ticking = false;
            };

            Debouncer.prototype = {
                constructor : Debouncer,
              
                /**
                 * dispatches the event to the supplied callback
                 * @private
                 */
                update : function() {
                  this.callback && this.callback();
                  this.ticking = false;
                },
              
                /**
                 * ensures events don't get stacked
                 * @private
                 */
                requestTick : function() {
                  if(!this.ticking) {
                    requestAnimationFrame(this.rafCallback || (this.rafCallback = this.update.bind(this)));
                    this.ticking = true;
                  }
                },
              
                /**
                 * Attach this as the event listeners
                 */
                handleEvent : function() {
                  this.requestTick();
                }
            };

            new Debouncer(func);
        };
    };
});

genosProviders.provider('sortTypes', function() {
    this.$get = function() {
        return  [
                    {title: 'Magnitude', stitle: 'Mag', type: 'magnitude'}, 
                    {title: 'MaxMagnitude', stitle: 'MaxMag', type: 'maxmag'}, 
                    {title: 'Frequency', stitle: 'Freq', type: 'freq'}, 
                    {title: 'GMAF', stitle: 'GMAF', type: 'gmaf'}, 
                    {title: 'Reference', stitle: 'Ref', type: 'numrefs'}, 
                    {title: 'Location', stitle: 'Loc', type: 'location'},
                    {title: 'Modification Date', stitle: 'Date', type: 'date'}
                ];
    };
});

genosProviders.provider('safeApply', function() {
    this.$get = function() {
        return function($scope, func) {
            if($scope.$$phase) {
                func()
            }
            else {
                if(func)
                    $scope.$apply(func)
                else
                    $scope.apply();
            }
        }; 
    };
});

genosProviders.provider('postBoring', function() {
    this.$get = function($http, $q) {
        return function(title) {
            var deferred = $q.defer(),
                postBoringPromise = $http.jsonp('http://www.snpedia.com/api.php?callback=JSON_CALLBACK&action=sfautoedit&form=Genotype&format=json&target='+title+'&Genotype[repute]=Good&Genotype[magnitude]=0&wpSummary=promethease+editor+set+boring')

            postBoringPromise.success(function(data) {
                deferred.resolve();
            });
            postBoringPromise.error(function(data) {                
                deferred.reject();
            });
            
            return deferred.promise;
        };
    }
});
