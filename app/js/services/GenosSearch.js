var fullproof = require('../../../vendor_libs/fullproof-english');

var GenosSearch = function(collectionService) {
    this.collectionService = collectionService;
    this.engine = new fullproof.BooleanEngine();
};

GenosSearch.prototype.start = function(callback, errorCallback, progressCallback, reset) {
    var self = this,
        dbName = "fpPrometheasedb",
        makeTextInitializer = function(progress) {
            var values = [],
                textArray = [];

            for(var i=0, data = self.collectionService.getDefaultCollection(), dL = data.length; i < dL; i++) {
                values.push(i);
                textArray.push((data[i].genobody ? data[i].genobody:'') +','+
                    (data[i].genosummary ? data[i].genosummary : '') + ',' +
                    (data[i].rstext ? data[i].rstext : '') + ',' +
                    (data[i].tags ? data[i].tags.join(','): '') + ',' +
                    (data[i].title ? data[i].title : '') + ',' +
                    (data[i].gene ? data[i].gene : '') + ',' +
                    (data[i].dbsnp ? data[i].dbsnp : '') + ',' +
                    (data[i].clinvar_disease ? data[i].clinvar_disease.replace(/;\s*/,',') : '')
                );
            }

            return function(injector, callback) {
                var synchro = fullproof.make_synchro_point(callback, data.length-1);
                injector.injectBulk(textArray, values, callback, progress);
                textArray = null;
            };
        },
        textIndex = new fullproof.IndexUnit("textindex",
                            new fullproof.Capabilities().setStoreObjects(false).setUseScores(false).setDbName(dbName),
                            new fullproof.StandardAnalyzer(fullproof.normalizer.to_lowercase_nomark, fullproof.normalizer.remove_duplicate_letters, fullproof.normalizer.filter_in_object),
                            makeTextInitializer(function(val) {
                                progressCallback(Math.ceil(val * 100));
                            }), null, reset);

        self.engine.open(textIndex,fullproof.make_callback(callback, true),fullproof.make_callback(callback, false));
};  

GenosSearch.prototype.lookup = function(term, callback) {
    var self = this;

    self.engine.lookup(term, function(resultset) {
      var resultArr = [];

      if(resultset.data) {
        resultset.data.forEach(function(index) {
          resultArr.push(self.collectionService.getDefaultCollection()[index]);
        });
      }

      callback(resultArr);
    });
};

GenosSearch.prototype.getAllKeys = function(callback) {
    this.engine.getAllKeys(function(resultset) {
        if(!resultset)
            return "no key found.";

        callback(resultset);
    });
};

GenosSearch.prototype.clear = function(callback) {
    this.engine.clear(callback);
};

module.exports = angular.module('GenosSearch',[]).factory('search', function(collectionService) {
    return (new GenosSearch(collectionService));
});
