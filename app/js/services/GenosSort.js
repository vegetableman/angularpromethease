/*
* Service to sort data
*/
var crossFilterSort = require('../../../vendor_libs/crossfilter-sort');

var GenosSort = function(sortTypes) {
    this.sortTypes = sortTypes;
    this.sortType = undefined;
    this.reverseSort = undefined;
};

GenosSort.prototype.startSort = function(collection, type, reverse) {
    var filterDimension, sortedCollection = [];

    if(type === 'location') {
        var chrom2int = function(chrom) {
                var chromint;

                if (chrom == 'X')
                    chromint = 23;
                else if (chrom === 'Y')
                    chromint = 24;
                else if (chrom === 'MT')
                    chromint = 25;
                else if (chrom === undefined)
                    chromint = 26;
                else
                    chromint = parseInt(chrom,10);

                return chromint;
            },
            pseudoposition = function (adict) {
                var scale = 1000000000,
                    out = chrom2int(adict['chrom']) * scale + adict['pos'];
                return out;
            };

        filterDimension = crossFilterSort.sort(collection).dimension(function(d) {
            var pseudopos = pseudoposition(d);
            return pseudopos ? pseudopos : 1;
        });

        sortedCollection = reverse ? filterDimension.bottom(Infinity) : filterDimension.top(Infinity);
    }
    else if(type === 'magnitude') {
        filterDimension = crossFilterSort.sort(collection).dimension(function(d) {
            return d['lindex'];
        });

        sortedCollection = reverse ? filterDimension.top(Infinity) : filterDimension.bottom(Infinity);
    }
    else {
        filterDimension = crossFilterSort.sort(collection).dimension(function(d) {
            return d[type] ? d[type] : (reverse ? Infinity: 0);
        });

        sortedCollection = reverse ? filterDimension.bottom(Infinity) : filterDimension.top(Infinity);
    }

    return sortedCollection;
};

GenosSort.prototype.sort = function(collection, type, reverse) {
    var self = this;

    if(collection.length < 2) 
        return collection;

    self.sortType = type || self.sortType || 'magnitude'; 
    self.reverseSort = typeof reverse === 'undefined' ? self.reverseSort : !!reverse;
	for(var i = 0, sTL = self.sortTypes.length; i < sTL; i++) {
      if(self.sortTypes[i].type === self.sortType) {
          collection = self.reverseSort ? self.startSort(collection, self.sortType, self.reverseSort) : self.startSort(collection, self.sortType);
          break;
      }
    }

    return collection;
};

module.exports = angular.module('GenosSort',[]).factory('sorter', function(sortTypes) {
    return new GenosSort(sortTypes);
});