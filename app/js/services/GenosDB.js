//A variant of localForage and fullproof for promethease - tinier with angular promises and supports bulk inject, key retrieval
//only indexedDB supported as of now
//@vegetableman

//Private Methods Starts ---
var indexedDB = indexedDB || window.indexedDB || window.webkitIndexedDB ||
									window.mozIndexedDB || window.OIndexedDB ||
									window.msIndexedDB;
var $q, 
	$timeout, 
	DBNAME = 'localDB';

var driver = {
	INDEXEDDB: indexedDBWrapper,
	WEBSQL: webSqlWrapper,
	storeNames: [],
	getDriver: function() {
		//get compatible driver
		if (indexedDB) {
			return new indexedDBWrapper();
		} else if (window.openDatabase) { // WebSQL is available, so we'll use that.
			return new webSqlWrapper();
		} else { // If nothing else is available, we return null.
			return null;
		}
	},
	setStores: function(storeArray){},
	bulkInject: function(dataArray, storeName, progress) {},
	getAllKeys: function(storeName) {},
	getByKey: function(key, storeName){},
	getAll: function(storeName) {},
	deleteDB: function(){},
	clear: function() {}
};


//Wrapper for WEBSQL
var webSqlWrapper = function(data) {
	this.db = window.openDatabase(DBNAME, 1, 'Promethease db', 1024 * 1024 * 5);


	this.setStores = function(storeArray) {
		var self = this,
			i = 0;

		var createStores = function() {
			self.db.transaction(function (tx) {
	        	tx.executeSql('CREATE TABLE IF NOT EXISTS '+ storeArray[i] +' (id VARCHAR(52) NOT NULL PRIMARY KEY, value)', [], function() {
	        		if(i === storeArray.length - 1)
	        			return;

	        		createStores(++i);
	        	}, function(err) {
	        		console.log(err + 'Create Table error');
	        	});
	    	}, null);
		};

		createStores();

		self.storeNames = storeArray;
	};

	this.getStores = function() {
		return this.storeNames;
	};

	this.bulkInject = function(dataArray, storeName) {
		var deferred = $q.defer();

		this.db.transaction(function(tx) {
			for(var i = 0, dL = dataArray.length; i < dL; i++) {
				var data = dataArray[i];
				tx.executeSql('INSERT INTO '+ storeName +' (id,value) VALUES (?,?)', [data.key, JSON.stringify(data)], function() {
					if(i === dataArray.length - 1)
						deferred.resolve();
				}, null);
			}
		});

		deferred.resolve(false);

		return deferred.promise;
	};

	this.getAll = function(storeName) {
		var deferred = $q.defer();

		this.db.transaction(function(tx) {
			tx.executeSql('SELECT value FROM '+ storeName, [], function(tx, result) {
				var collection = [];
				
				for(var i=0, rows = result.rows, rL = rows.length; i < rL; i++) {
					collection.push(JSON.parse(rows.item(i).value));
				}

				deferred.resolve(collection);
			}, function(err) {
				console.log('Error SELECT Table' + err);
			});
		});	

		return deferred.promise;
	};

	this.clear = function() {
		var self = this,
			deferred = $q.defer(),
			storeArray = this.getStores(),
			i = 0,
			clearStores = function() {
				var store = storeArray[i];
				
				self.db.transaction(function(tx) {
					tx.executeSql('DELETE FROM '+ store, [], function() {
						if(i === storeArray.length - 1)
							deferred.resolve();
						else
							clearStores(++i);
					}, null);
				}, function(err) {
					console.log(err+ 'Error Clearing db');
				});
			};

		clearStores();

		return deferred.promise;
	};
};

//Wrapper for INDEXEDDB
var indexedDBWrapper = function(data) {
	this.setStores = function(storeArray) {
		this.storeNames = storeArray;
	};

	this.getStores = function() {
		return this.storeNames;
	};

	this.withStore = function(storeName, type, f) {
		var self = this;

		if(self.db) {
			 f(self.db.transaction(storeName, type).objectStore(storeName));
		}
		else {
			var req = indexedDB.open(DBNAME, 1);
			req.onerror = function() {
				console.error('indexedDB: can\'t open database:', req.error.name);
			};
			req.onupgradeneeded = function(ev) {
				var db = ev.target.result;
				var stores = self.getStores();
				for(var i=0, sc = stores.length; i < sc ; i++) {
					if (!db.objectStoreNames.contains(stores[i]))
						db.createObjectStore(stores[i], {keyPath: "key"});
				}
			};
			req.onsuccess = function(ev) {
				self.db = ev.result || ev.target.result;
				f(self.db.transaction(storeName, type).objectStore(storeName));
			};
		}
	};

	this.bulkInject = function(dataArray, storeName, progress) {
		var deferred = $q.defer();

		this.withStore(storeName, 'readwrite', function(store) {
			var i = 0,
				progressOffset = 0,
				putData = function() {
					//callback with progress
					$timeout(function() {
							if(dataArray && progress)
								progress(progressOffset/dataArray.length);
					}, 1, false);

					if(i < dataArray.length) {
						progressOffset += 100;
						store.put(dataArray[i]).onsuccess = putData;
						i++;
					}
					else {
						deferred.resolve(true);
					}
				};

			putData();
		});

		return deferred.promise;
 	};

	this.getAll = function(storeName) {
		var deferred = $q.defer(),
			dataArray = [];

		 this.withStore(storeName, 'readonly', function(store) {
			var oreq = store.openCursor();
			oreq.onsuccess = function(event) {
				var cursor = event.target.result;
				if(cursor) {
					dataArray.push(cursor.value);
					cursor.continue();
				}
				else {
					deferred.resolve(dataArray);
				}
			};
			oreq.onerror = function() {
				deferred.reject();
			};
		 });

		 return deferred.promise;
 	};

   	this.clear = function() {
		var self = this,
			deferred = $q.defer(),
			stores = self.getStores(),
			i = 0,
			clearStores = function(i) {
				if(i < stores.length) {

					self.withStore(stores[i], 'readwrite', function(store) {
						var req = store.clear();
						req.onsuccess = clearStores(++i);
						req.onerror = function() {
							deferred.reject();
						};
					});

				}
				else {
					deferred.resolve();
				}
			};

		clearStores(i);
		return deferred.promise;
 	};

	this.deleteDB = function() {
		var deferred = $q.defer();

		if(this.db && db.close)
			db.close();

		var req = indexedDB.deleteDatabase(DBNAME);
		req.onsuccess = function() {
			deferred.resolve();
		};
		req.onerror = function() {
			deferred.reject();
		};

		return deferred.promise;
	};

	// @notused
	this.getByKey = function(key, storeName) {
		var deferred = $q.defer();

		this.withStore(storeName, 'readonly', function getItemBody(store) {
			var req = store.get(key);

			req.onsuccess = function getItemOnSuccess() {
				deferred.resolve(req.result);
			};

			req.onerror = function(event) {
				deferred.reject();
			};
		 });

		return deferred.promise;
	 };

};


//set driver as super class
indexedDBWrapper.prototype = driver;
webSqlWrapper.prototype = driver;

//Private Methods Ends ---

//Public Methods Starts -----
var GenosDB = function(m$q, m$timeout) {
	$q = m$q;
	$timeout = m$timeout;
};

GenosDB.prototype.init = function(dbName) {
	DBNAME = dbName;
	this.driver = driver.getDriver();
	return this;
};

GenosDB.prototype.setIndexes = function(indexArray) {
	if(this.driver)
		this.driver.setStores(indexArray);
	return this;
};

GenosDB.prototype.bulkInject = function(dataArray, indexName, callback, errorCallback, progressCallback) {
	if(this.driver)
		this.driver.bulkInject(dataArray, indexName, progressCallback).then(callback, errorCallback);
};

GenosDB.prototype.setItem = function(item, callback, errorCallback) {
	if(this.driver)
		this.driver.setItem(item).then(callback, errorCallback);
};

GenosDB.prototype.getAll = function(indexName, callback, errorCallback) {
	if(this.driver)
		this.driver.getAll(indexName).then(callback, errorCallback);
	else
		callback();
};

GenosDB.prototype.getAllKeys = function(indexName, callback, errorCallback) {
	if(this.driver)
		this.driver.getAllKeys(indexName).then(callback, errorCallback);
	else
		callback();
};

GenosDB.prototype.getByKey = function(key, indexName, callback, errorCallback) {
	if(this.driver)
		this.driver.getByKey(key, indexName).then(callback, errorCallback);
	else
		callback();
};

GenosDB.prototype.setLocalItem = function(key, value) {
	localStorage.setItem(key, value);
};

GenosDB.prototype.getLocalItem = function(key) {
	return localStorage.getItem(key);
};

GenosDB.prototype.isStored = function() {
	var args = Array.prototype.slice.call(arguments);

	 for(var i=0, al = args.length; i < al; i++) {
		 if(!this.getLocalItem(args[i]))
			 return false;
	 }
	 return true;
};

GenosDB.prototype.clear = function(callback, errorCallback) {
	var self = this;

	localStorage.clear();

	try {
	    if(!self.driver) {
	        callback()
	    }
	    else {
		    self.driver.clear().then(callback, errorCallback);
	    }
	} 
	catch(ex) {
		if(self.driver && self.driver.deleteDB)
			self.driver.deleteDB().then(callback, errorCallback);
	}
};

//Public Methods Ends -----

module.exports = angular.module('GenosDB',[]).factory('db', function($q, $timeout) {
	return new GenosDB($q, $timeout);
});

