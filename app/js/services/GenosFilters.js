//var Bloodhound = require('../../../vendor_libs/bloodhound/Bloodhound');
var Bloodhound = require('../../../vendor_libs/bloodhound/bloodhound.js');


var GenosFilters = function(db, $q, isMobile) {
    this.db = db.init('Prometheasedb').setIndexes(['tags', 'clinvar']);
    this.$q = $q;
    this.isMobile = isMobile;

    //range values
    this.lowMagnitude = -Infinity; 
    this.highMagnitude = Infinity; 
    this.lowRef = -Infinity; 
    this.highRef = Infinity;

    //on/off values
    this.isDragMagnitude = false;
    this.isDragRef = false;
    this.showSNPs = true;
    this.showGenosets = true; 
    this.showGood = true;
    this.showBad = true;
    this.showNotSet = true;

    //on/off clinvar modes
    this.showPatho = false;
    this.showPPatho = false;
    this.showPNPatho = false;
    this.showNPatho = false;
    this.showUntested = false;
    this.showOther = false;
    this.showDrugR = false;
    this.showHistoC = false;

    this.clinvarMode = false;

    //objects
    this.tagItem = {};
    this.clinvarItem = {};

    //collections
    this.filterValues = []; 
    this.tagCache = [];
    this.clinvarCache = [];
    this.tagCollection = [];
    this.clinvarCollection = [];

    //engine
    this.tagEngine = undefined;
    this.clinvarEngine = undefined;
    this.values = undefined;

    //counts
    this.badReputeCount = 0;
    this.goodReputeCount = 0;
};

GenosFilters.prototype.filter = function(collection, values) {
    var self = this,
        goodRepute = 0,
        badRepute = 0,
        resultCollection = [];

    if(!self.values && !values) return collection;

    collection = self.setFilterValues(collection, self.values = (values || self.values));

     //apply filters based on user set data
    for(var i=0, count = collection.length; i<count; i++) {

        var genos = collection[i],
            magnitude = genos.magnitude ? parseFloat(genos.magnitude): undefined,
            numRef = genos.numrefs ? parseInt(genos.numrefs): undefined,
            doAdd = true,
            clinvar_1,
            repute;


        if(self.clinvarMode) {
            doAdd = ((clinvar_1 = genos.clinvar_1) &&
                            (((clinvar_1 && +clinvar_1 === 1) && self.showUntested) ||
                            ((clinvar_1 && +clinvar_1 === 2) && self.showNPatho) ||
                            ((clinvar_1 && +clinvar_1 === 3) && self.showPNPatho) ||
                            ((clinvar_1 && +clinvar_1 === 4) && self.showPPatho) ||
                            ((clinvar_1 && +clinvar_1 === 5) && self.showPatho) ||
                            ((clinvar_1 && +clinvar_1 === 6) && self.showDrugR) ||
                            ((clinvar_1 && +clinvar_1 === 7) && self.showHistoC) ||
                            ((clinvar_1 && +clinvar_1 === 255) && self.showOther))) ? true: false;
        }
       

       doAdd = (doAdd && (!magnitude || (magnitude && (magnitude >= self.lowMagnitude && magnitude <= self.highMagnitude)))) ? true: false;
       
       doAdd = (doAdd && (!numRef || (numRef && (numRef >= self.lowRef && numRef <= self.highRef)))) ? true: false;
       
       doAdd = (doAdd && (((genos.rsnum || genos['23andMe SNP']) && self.showSNPs) ||
                          ((genos.title && !genos.rsnum && !genos['23andMe SNP']) && self.showGenosets))) ? true: false;
       
       doAdd = (doAdd && ((((repute = genos.repute) && repute.toLowerCase() === 'good') && self.showGood) ||
                          ((repute && repute.toLowerCase() === 'bad') && self.showBad) ||
                          (!repute && self.showNotSet))) ? true: false;


        if(doAdd) {
            resultCollection.push(genos);
        }
    }

    return resultCollection;
};

GenosFilters.prototype.setFilterValues = function(collection, values) {
    var self = this;

    self.isDragMagnitude = values[0];
    self.isDragRef = values[3];

    if(self.isDragMagnitude || self.isDragRef) 
        return collection;

    self.lowMagnitude =  parseFloat(values[1]);
    self.highMagnitude = parseFloat(values[2]);
    self.lowRef = parseInt(values[4]);
    self.highRef = parseInt(values[5]);

    self.showSNPs = values[6];
    self.showGenosets = values[7];
    self.showGood = values[8];
    self.showBad = values[9];
    self.showNotSet = values[10];

    self.showPatho = values[11];
    self.showPPatho = values[12];
    self.showPNPatho = values[13];
    self.showNPatho = values[14];
    self.showUntested = values[15];
    self.showOther = values[16];
    self.showDrugR = values[17];
    self.showHistoC = values[18];

    if(self.showPatho || self.showPPatho || self.showNPatho || self.showOther 
        || self.showUntested || self.showHistoC || self.showDrugR || self.showPNPatho)
        self.clinvarMode = true;
    else
        self.clinvarMode = false;

    self.tagItem = values[19];
    self.clinvarItem = values[20];

    var tCollection, cCollection, tcCollection;

    if(self.tagItem) {
        tCollection = [];
        //in case of tag selected from block view
        if(typeof self.tagItem === 'string') {
            for(var i =0, tgCollection = self.tagCollection, tL = tgCollection.length; i < tL; i++)
                if(self.tagItem === tgCollection[i].key) {
                    self.tagItem = tgCollection[i];
                    break;
                }
        }
        
        var tagMatchArr = self.tagItem.v;

        for(var i = 0, mL = collection.length; i < mL; i++) {
          if(tagMatchArr.indexOf(collection[i].lindex) > -1)
            tCollection.push(collection[i]);
        }
    }
    
    if(self.clinvarItem) {
        cCollection = []

        var clinvarMatchArr = self.clinvarItem.v;

        for(var i = 0, mL = collection.length; i < mL; i++) {
          if(clinvarMatchArr.indexOf(collection[i].lindex) > -1)
            cCollection.push(collection[i]);
        }
    }

    //both filters active
    if(tCollection && tCollection.length && cCollection && cCollection.length) {
        tcCollection = [];

        for(var i=0, tL = tCollection.length; i < tL; i++) {
            for(var j=0, cL = cCollection.length; j < cL; j++) {
                if(tCollection[i].lindex === cCollection[j].lindex) {
                    tcCollection.push(tCollection[i]);
                    continue;
                }
            }
        }
    }
    
    if(self.tagItem && self.clinvarItem) {
        collection = tcCollection;
    }
    //tag active
    else if(tCollection) {
        collection = tCollection;
    }
    //clinvar active
    else if(cCollection) {
        collection = cCollection;
    }

    return collection;
};


GenosFilters.prototype.setMaxMagnitude = function(maxmag) {
    this.maxMagnitude = maxmag;
    this.db.setLocalItem('maxMagnitude', maxmag);
};

GenosFilters.prototype.setMinMagnitude = function(minmag) {
    this.minMagnitude = minmag;
    this.db.setLocalItem('minMagnitude', minmag);
};

GenosFilters.prototype.getMaxMagnitude = function() {
    return this.db.getLocalItem('maxMagnitude');
};

GenosFilters.prototype.getMinMagnitude = function() {
    return this.db.getLocalItem('minMagnitude');
};

GenosFilters.prototype.setMinRef = function(minref) {
    this.minRef = minref;
    this.db.setLocalItem('minRef', minref);
};

GenosFilters.prototype.setMaxRef = function(maxref) {
    this.maxRef = maxref;
    this.db.setLocalItem('maxRef', maxref);
};

GenosFilters.prototype.getMinRef = function() {
    return this.db.getLocalItem('minRef');
};

GenosFilters.prototype.getMaxRef = function() {
    return this.db.getLocalItem('maxRef');
};

GenosFilters.prototype.getTagCollection = function() {
    var self = this,
        deferred = self.$q.defer();

    var successCallback = function(collection) {
        self.tagCollection = collection;
        deferred.resolve(self.tagCollection);
    },
    errorCallback = function() {
        console.log('Error on Clinvar Fetch');
        deferred.reject();
    };

    if(self.tagCollection.length)
        deferred.resolve(self.tagCollection);
    else
        self.db.getAll('tags', successCallback, errorCallback);

    return deferred.promise;
};

GenosFilters.prototype.getClinvarCollection = function() {
    var self = this,
        deferred = self.$q.defer();

    var successCallback = function(collection) {
        self.clinvarCollection = collection;
        deferred.resolve(self.clinvarCollection);
    },
    errorCallback = function() {
        console.log('Error on Clinvar Fetch');
        deferred.reject();
    };

    if(self.clinvarCollection.length)
        deferred.resolve(self.clinvarCollection);
    else
        self.db.getAll('clinvar', successCallback, errorCallback);

    return deferred.promise;
};

GenosFilters.prototype.setClinvarCollection = function(collection) {
    var self = this,
        deferred = self.$q.defer();

    self.clinvarCollection = collection;

    var successCallback = function() {
        console.log('Clinvars Indexed');
        deferred.resolve();
    },
    errorCallback = function() {
        console.log('Error on Clinvar Indexing');
        deferred.reject();
    };

    self.isMobile(function(value) {
        if(!value)
            self.db.bulkInject(collection, 'clinvar', successCallback, errorCallback);
    });

    return deferred.promise;
};

GenosFilters.prototype.setTagCollection = function(collection, uniqueIdx) {
    var self = this,
        deferred = self.$q.defer();  

    self.tagCollection = collection;

    var successCallback = function() {
        console.log('Tags Indexed');
            //if new data has been added or opened with different url, reload index;
        self.db.setLocalItem('indexed', uniqueIdx);
        deferred.resolve();
    },
    errorCallback = function() {
        console.log('Error on Tag Indexing');
        deferred.reject();
    };

    self.isMobile(function(value) {
        if(!value)
            self.db.bulkInject(collection, 'tags', successCallback, errorCallback);
        else
            self.db.setLocalItem('indexed', uniqueIdx);
    });

    return deferred.promise;
};

GenosFilters.prototype.loadTagSuggestions = function(collection) {
    this.tagEngine = new Bloodhound({
        datumTokenizer: function(d) {
          return Bloodhound.tokenizers.whitespace(d.key);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: collection
    });

    this.tagEngine.initialize();
};

GenosFilters.prototype.loadClinvarSuggestions = function(collection) {
    this.clinvarEngine = new Bloodhound({
        datumTokenizer: function(d) {
          return Bloodhound.tokenizers.whitespace(d.key);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: collection
    });

    this.clinvarEngine.initialize();
};

GenosFilters.prototype.showTagSuggestion = function(tagCollection, value) {
    var self = this;

    if(value && value.length > 0)   {
      if(self.tagEngine) {
            self.tagEngine.ttAdapter(value, function(result) {
                var keyArr = [],
                    r = result || [];
                
                for(var i=0, rL = r.length; i < rL; i++) {
                    keyArr.push(r[i].key);
                }

                for(var i=0, tcL = tagCollection.length; i < tcL; i++) {
                    var tag = tagCollection[i];
                    tag.active = keyArr.indexOf(tag.key) >= 0;
                }

                return r;  
            });
        }
    }
    else {
      for(var i=0, tcL = tagCollection.length; i < tcL; i++) {
        tagCollection[i].active = true;
      }
    }
};

GenosFilters.prototype.showClinvarSuggestion = function(clinvarCollection, value) {
    var self = this;

    if(value && value.length > 0) {
      if(self.clinvarEngine) {
            self.clinvarEngine.ttAdapter(value, function(result) {
                var keyArr = [];
                
                for(var i=0, rL = result.length; i < rL; i++) {
                    keyArr.push(result[i].key);
                }

                for(var i=0, tcL = clinvarCollection.length; i < tcL; i++) {
                    var clinvar = clinvarCollection[i];
                    clinvar.active = keyArr.indexOf(clinvar.key) >= 0;
                }

                return result;  
            });
        }
    }
    else {
      for(var i=0, tcL = clinvarCollection.length; i < tcL; i++) {
        clinvarCollection[i].active = true;
      }
    }
};

GenosFilters.prototype.getClinvarByKey = function(key) {
    var self = this
        deferred = self.$q.defer();

    self.getClinvarCollection().then(function(collection) {
        for(var i =0, cL = collection.length; i < cL; i++)
            if(key === collection[i].key) {
                deferred.resolve(collection[i]);
                break;
            }
    });

    return deferred.promise;
};

GenosFilters.prototype.getTagByKey = function(key) {
    var self = this
        deferred = self.$q.defer();

    self.getTagCollection().then(function(collection) {
        for(var i =0, cL = collection.length; i < cL; i++)
            if(key === collection[i].key) {
                deferred.resolve(collection[i]);
                break;
            }
    });

    return deferred.promise;
};

GenosFilters.prototype.clear = function(callback) {
    this.db.clear(callback);
};

module.exports = angular.module('GenosFilters',[]).factory('filterService', function(db, $q, isMobile) {
    return (new GenosFilters(db, $q, isMobile));
});