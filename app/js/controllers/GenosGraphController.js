var GenosGraphController;
	
exports.GenosGraphController = GenosGraphController = function($scope, collectionService, safeApply) {
	this.scope = $scope;
	this.collectionService = collectionService;
	this.safeApply = safeApply;

	this.setupControls();
	this.setUpGraphBehaviour();
	this.onGraphUpdate();
}

GenosGraphController.prototype.setUpGraph = function(newValue) {
	var self = this,
		graph = {
			nodes: [],
			edges: []
		},
		nodeIndex = 0,
		edgeIndex = 0,
		tagMap = {},
		collection = newValue || self.collectionService.getActiveCollection(),
		 //@fixme hack - react-angular interaction pain
		 //get limit or number of items to display from tab selected
		limit = self.scope.main.viewIndex === 0 ? document.querySelectorAll('.genos-item').length:document.querySelectorAll('.genos-table-r').length;

	for (i = 0, cL = Math.min(limit, collection.length); i < cL; i++) {
		var genos = collection[i],
			parentNodeIndex = nodeIndex++;

		//add genos
		graph.nodes.push({
			id: 'n' + parentNodeIndex,
			index: genos.lindex,
			label: genos.title,
			x: Math.random() * limit,
			y: Math.random() * limit,
			size: genos.magnitude ? genos.magnitude * 0.5 : 0.2,
			color: genos.color,
			freqcolor: genos.freqcolor,
			freq: genos.freq,
			tag: false
		});

		//add tags
		if(genos.tags) {
			var tags = genos.tags;

			for (j = 0, tL = tags.length; j < tL; j++) {	
				var tag = tags[j];

				if(!tagMap[tag]) {
					var gc = genos.repute === 'Good' ? 1: 0,
						bc = genos.repute === 'Bad' ? 1: 0,
						nc = !genos.repute ? 1: 0;

					tagMap[tag] = nodeIndex++;
					graph.nodes.push({
						id: 'n' + tagMap[tag],
						label: tag,
						x: Math.random() * limit,
						y: Math.random() * limit,
						size: 0.3,
						color: 'steelblue',
						good: gc,
						bad: bc,
						notSet: nc,
						total: gc + bc + nc, 
						tag: true
					});
				}
				else {
					var node = graph.nodes[tagMap[tag]],
						gc = node.good,
						bc = node.bad,
						nc = node.notSet; 

					node.good = genos.repute === 'Good' ? ++gc: node.good;
					node.bad = genos.repute === 'Bad' ? ++bc: node.bad;
					node.notSet = !genos.repute ? ++nc: node.notSet;
					node.total = node.good + node.bad + node.notSet;
				}

				graph.edges.push({
					id: 'e' + edgeIndex++,
					source: 'n' + parentNodeIndex,
					target: 'n' + tagMap[tag],
					size: 10,
					color: genos.repute === 'Good'? '#0CC00C': genos.repute === 'Bad' ? '#F55151' : '#CCC',
					type: 'arrow'
				});
			}
		}
	}

	self.safeApply(self.scope, function() {
	 	self.genos = null;
		self.graph = graph;
	 });
};

//Controls below the graph
GenosGraphController.prototype.setupControls = function() {
	var self = this;

	self.zoomIn = function() {
		self.scope.$broadcast('zoomIn');
	};

	self.zoomOut = function() {
		self.scope.$broadcast('zoomOut');
	};

	self.resetView = function() {
		self.scope.$broadcast('resetView');
	};

	self.prettifyGraph = function() {
		self.prettify = !self.prettify;
		self.scope.$broadcast('prettifyView', self.prettify);
	};

	self.information = '<div class="graph-info-item"><span class="yellow-node node"></span> - Genoset</div>\
						  <div class="graph-info-item"><span class="red-node node"></span> - SNP</div>\
						  <div class="graph-info-item"><span class="blue-node node"></span> - Tag</div>\
						  <div class="graph-info-item"><span class="green-edge edge"></span> - Good Repute</div>\
						  <div class="graph-info-item"><span class="red-edge edge"></span> - Bad Repute</div>\
						  <div class="graph-info-item"><span class="gray-edge edge"></span> - No Repute</div>\
						  <div class="graph-info-item">Larger circles indicate higher magnitude.</div>';

};

GenosGraphController.prototype.setUpGraphBehaviour = function() {
	var self = this;

	self.showNode = function(item) {
		self.scope.$broadcast('nodeActivity', item, 'emulateOverNode');
	};

	self.resetNodes = function() {
		self.scope.$broadcast('nodeActivity', null, 'resetNodes');
	};

	self.setNode = function(item, id) {
		self.nodeItemId = id;
		self.scope.$broadcast('nodeActivity', item, 'emulateClickNode');
	};

	self.toggleTab = function(type) {
		if(type === 'tag') {
			self.nodeiteminfo = '<div class="graph-info-item">The bars indicate repute of the tag for the current result. </div>\
								<div class="graph-info-item">Red: Bad Repute.</div>\
								<div class="graph-info-item">Green: Good Repute.</div>\
								<div class="graph-info-item">Gray: No Repute.</div>';

			self.showTag = !self.showTag;
			self.showTitle = false;
		}
		else {
			self.nodeiteminfo = '<div class="graph-info-item">The colors indicate frequency. The more red in color, the more rare this genotype.</div>';
			self.showTitle = !self.showTitle;
			self.showTag = false;	
		}
	};

	self.hideNodeList = function() {
		self.showTag = false;
		self.showTitle = false;
	};
};

GenosGraphController.prototype.onGraphUpdate = function() {
	var self = this;

	self.scope.$watchCollection(function() { 
		return self.collectionService.getActiveCollection();
    }, function(newValue, oldValue) {
    	if(!scope.main.doShowGraph) {
    		return;
    	}

    	document.querySelector('.view-loader').style.visibility = 'visible';

	    self.setUpGraph(newValue);

    	if(!newValue.length)
    		document.querySelector('.view-loader').style.visibility = 'hidden';
    });

    self.scope.$watch('main.doShowGraph', function(newValue, oldValue) {
    	if(newValue === oldValue) return;
    	if(newValue) {
	    	self.setUpGraph();
    	}
    	else {
    		document.querySelector('.view-loader').style.visibility = 'hidden';
    	}
    });
};

GenosGraphController.$inject = ['$scope', 'collectionService', 'safeApply'];
