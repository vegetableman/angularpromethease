/*
* Base Controller
* @vegetableman
*/

var GenosController;

exports.GenosController = GenosController =  function($scope, collectionService, safeApply, sortTypes, filterService, postBoring, debounce)  {
    this.scope = $scope;
    this.collectionService = collectionService;
    this.filterService = filterService;
    this.sortTypes = sortTypes;
    this.postBoring = postBoring;
    this.safeApply = safeApply;
    this.loading = true;
	this.closeWelcomeScreen = false;
	this.currentDate=new Date();
    this.debounce = debounce;

    //methods
    this.setUpScope();
    this.initializeCollection();
    this.setUpViewTypes();
    this.hideLoading();
    this.setUpSorting();
    this.setUpFilterTypes();
    this.setUpFilterSuggestions();
    this.setUpModeFilters();
    this.setUpModes();
    this.setUpSearch();
    this.setUpNoMatch();
    this.setUpBoring();
    this.setUpCount();
    this.setUpReset();
};

GenosController.prototype.setUpScope = function() {
    var self = this;
    self.scope.dialog = {};
};

GenosController.prototype.initializeCollection = function() {
    var self = this;
    self.collectionService.setUpCollection();
};

GenosController.prototype.setUpSorting = function() {
    var self = this,
        sortType;

    self.sortBy = function(type, index) {
        self.sortIndex = index >= 0 ? index : self.sortIndex;
        self.collectionService.setIsSort(true);
        self.collectionService.sortCollection((sortType = type || sortType), self.isReverse);
    };

    self.reverseSort = function() {
        self.isReverse = !self.isReverse;
        self.sortBy();
    }
};

GenosController.prototype.setUpFilterTypes = function() {
    var self = this;

    self.filter = {};

    self.filter.lowSelMagnitude = +self.filterService.getMinMagnitude() || 0;  //change string to number and avoid NaN values
    self.filter.highSelMagnitude = +self.filterService.getMaxMagnitude() || 0;
    self.filter.maxMagnitude = +self.filterService.getMaxMagnitude() || 0;
    self.filter.minMagnitude = +self.filterService.getMinMagnitude() || 0;

    self.filter.lowSelRef = +self.filterService.getMinRef() || 0;
    self.filter.highSelRef = +self.filterService.getMaxRef() || 0;
    self.filter.maxRef = +self.filterService.getMaxRef() || 0;
    self.filter.minRef = +self.filterService.getMinRef() || 0;

    self.toggleSearchMenu = function() {
        self.safeApply(self.scope, function() {
            self.toggleSearch = !self.toggleSearch;
            self.toggleControl = false;
            self.scope.dialog.index = undefined;
        });
    };

    self.toggleControlMenu = function() {
        self.safeApply(self.scope, function() {
            self.toggleControl = !self.toggleControl;
            self.toggleSearch = false;
            self.scope.dialog.index = undefined;
        });
    };

    self.scope.$on('closeMobileMenu', function(event) {
        if(self.toggleControl || self.toggleSearch)
            self.safeApply(self.scope, function() {
                self.toggleControl = false;
                self.toggleSearch = false;
            });
    });

    self.filterTypes = [
      {title: 'ClinVar Diseases', stitle: 'Clinvar', type: 'clinvar', helpText: '<div style="margin-bottom: 5px"><b>Filter by ClinVar Diseases.</b></div> Based on the ClinVar database.', getIsActive: function() {
        return (self.filter.clinvarActive === true);
      }},
      {title: 'Tags', type: 'tag', stitle: 'Tags', helpText: '<div style="margin-bottom: 5px"><b>Filter by Tags.</b></div> All the relevant words found on the report.', getIsActive: function() {
        return (self.filter.tagActive === true);
      }},
      {title: 'Magnitude', type: 'magnitude', stitle: 'Mag', helpText: '<div style="margin-bottom: 5px"><b>Filter by Magnitude.</b></div> 0 = Boring. 1 = Unset. 2+ = Interesting', getIsActive: function() {
        return (self.filter.lowSelMagnitude > self.filter.minMagnitude || self.filter.highSelMagnitude < self.filter.maxMagnitude)
      }},
      {title: 'Reference', type: 'numrefs', stitle: 'Ref', helpText: '<div style="margin-bottom: 5px"><b>Filter by Reference.</b></div>The number of published papers mentioning a SNP. Does not filter Genosets.', getIsActive: function() {
        return (self.filter.lowSelRef > self.filter.minRef || self.filter.highSelRef < self.filter.maxRef);
      }},
      {title: 'SNP/Genosets', type: 'snpgenoset', stitle: 'SNP/Geno', helpText: '<div style="margin-bottom: 5px"><b>Filter by SNP/Genosets.</b></div><div>SNP = Starts with rs###. Location of changes in the DNA.</div> <div>Genoset = Starts with gs###. They are combinations of multiple SNP\s.</div>', getIsActive: function() {
        return (self.filter.showSNPs === false || self.filter.showGenosets === false)
      }},
      {title: 'Repute', type: 'repute', stitle: 'Rep', helpText: '<div style="margin-bottom: 5px"><b>Filter by Repute.</b></div>Shows Good, Bad or Not Set.', getIsActive: function() {
        return (self.filter.showGood === false || self.filter.showBad === false || self.filter.showNotSet === false);
      }}
    ];

    self.isActive = function(type) {
      for(var i=0, ftL = self.filterTypes.length; i < ftL; i++) {
        var filterType = self.filterTypes[i];
        if(filterType.type === type)
          return filterType.getIsActive();
      }
    };

    self.scope.$watch('[main.filter.isDragMagnitude, main.filter.lowSelMagnitude, main.filter.highSelMagnitude, \
                                  main.filter.isDragRef, main.filter.lowSelRef, main.filter.highSelRef, \
                                  main.filter.showSNPs, main.filter.showGenosets, main.filter.showGood, main.filter.showBad, main.filter.showNotSet, \
                                  main.filter.showPatho, main.filter.showPPatho, main.filter.showPNPatho, main.filter.showNPatho, main.filter.showUntested, \
                                  main.filter.showOther, main.filter.showDrugR, main.filter.showHistoC, \
                                  main.filter.tagItem, main.filter.clinvarItem]',
    function(newValues, oldValues) {
        if(newValues === oldValues)
            return;

        self.collectionService.filterCollection(newValues);
    }, true);
},

GenosController.prototype.setUpFilterSuggestions = function() {
    var self = this;

    self.filter.loadClinvarSuggestions = function() {
        window.setTimeout(function() {
            self.filterService.getClinvarCollection().then(function(collection) {
                self.filterService.loadClinvarSuggestions(self.filter.clinvarCollection = collection);
            });
        }, 10);
    };

    self.filter.loadTagSuggestions = function() {
        window.setTimeout(function() {
            self.filterService.getTagCollection().then(function(collection) {
                self.filterService.loadTagSuggestions(self.filter.tagCollection = collection);
            });
        }, 10);
    };

    self.filter.selectTagSuggestion = function(value) {
        self.filter.tagItem = value;
        self.filter.tagActive = !!value;
    };

    self.filter.selectClinvarSuggestion = function(value) {
        self.filter.clinvarItem = value;
        self.filter.clinvarActive = !!value;
    };

    self.filter.showTagSuggestion = function(value) {
        if(self.filter.tagCollection)
           self.filterService.showTagSuggestion(self.filter.tagCollection, value);
    };

    self.filter.showClinvarSuggestion = function(value) {
        if(self.filter.clinvarCollection)
           self.filterService.showClinvarSuggestion(self.filter.clinvarCollection, value);
    };

    //on click of tag/clinvar on block view items
    self.scope.$on('filterByTag', function(event, value) {
        self.filterService.getTagByKey(value).then(function(tagItem) {
            self.filter.tagTerm = value;
            self.filter.selectTagSuggestion(tagItem);
        });
    });

    self.scope.$on('filterByClinvar', function(event, value) {
        self.filterService.getClinvarByKey(value).then(function(clinvarItem) {
            self.filter.clinvarTerm= value;
            self.filter.selectClinvarSuggestion(clinvarItem);
        });
    });
};

GenosController.prototype.setUpModeFilters = function() {
    var self = this;

    self.modeFilterTypes = [
        {title: 'All', type: 'all', desc: 'Show Everything', getIsActive: function() {
            return !(self.filter.showPatho || self.filter.showPPatho || self.filter.showPNPatho || self.filter.showNPatho || self.filter.showUntested ||
                                  self.filter.showOther || self.filter.showDrugR || self.filter.showHistoC);
        }},
        {title: 'ClinVar', type: 'clinvar', desc: 'Show only ClinVar Related Genos', getIsActive: function() {
            return (self.filter.showPatho || self.filter.showPPatho || self.filter.showPNPatho || self.filter.showNPatho || self.filter.showUntested ||
                                  self.filter.showOther || self.filter.showDrugR || self.filter.showHistoC);
        }},
    ];

    self.showModeFilter = function(type, index) {
        if(index === 0)
            self.safeApply(self.scope, function() {
                self.filter.showPatho = self.filter.showPPatho = self.filter.showPNPatho = self.filter.showNPatho = self.filter.showUntested =
                                  self.filter.showOther = self.filter.showDrugR = self.filter.showHistoC = false;
            });
    };

    self.isModeFilterActive = function(type) {
      for(var i=0, mtL = self.modeFilterTypes.length; i < mtL; i++) {
        var modeType = self.modeFilterTypes[i];
        if(modeType.type === type)
          return modeType.getIsActive();
      }
    }

};

GenosController.prototype.setUpModes = function() {
    var self = this;

    self.mode = {};

    self.isModeActive = function() {
        return self.mode.editorMode || self.mode.helpMode || self.mode.graphMode;
    };

    self.scope.$watch('main.mode.editorMode', function(newvalue, oldvalue) {
        if(newvalue === oldvalue) return;

        self.scope.$broadcast("editorMode", newvalue);
    });

    self.scope.$watch('main.mode.graphMode', function(newvalue, oldvalue) {
        if(newvalue === oldvalue) return;

        if(!newvalue) {
            self.safeApply(self.scope, function() {
                self.doShowGraph = newvalue;
            });
            if(self.viewIndex === 0) {
                self.showView('block', 0);
            }
            else {
                self.showView('table', 1);
            }
        }
        else {
            document.querySelector('.view-loader').style.visibility = 'visible';
            self.safeApply(self.scope, function() {
                self.startGraph = true;
            });
            setTimeout(function() {
                self.safeApply(self.scope, function() {
                    self.doShowGraph = newvalue;
                });
            }, 50);
        }
    });
};

//Fullproof Text Search Indexing and Search
GenosController.prototype.setUpSearch = function() {
    var self = this;

    //@notused
    self.createTextIndex = function(callback) {
        self.search.start(function() {
            console.log('Text Indexed');
            callback();
        },
        function() {
            console.log('Indexing error');
        },
        function(value) {
            if(value) {
                self.safeApply(self.scope, function() {
                    self.searchProgress = value;
                });
            }
        },
        self.resetIndex
    );
    };

    self.searchProgress = 0; self.resetIndex = false; self.searchTerm = '';

    self.searchGenos = function() {
        var term = self.searchTerm;

        if(term && term.length) {
            self.debounce(function() {

                var searchCollection = [];
                for(var i = 0, collection = self.collectionService.getCollection(), length = collection.length; i < length; i++) {
                    var genos = collection[i],
                        match;

                    var regExp = new RegExp(term, 'i');
                    match = genos.title && genos.title.toString().match(regExp);
                    match = !!match || genos.tags && genos.tags.toString().match(regExp);
                    match = !!match || genos.clinvar_disease && genos.clinvar_disease.toString().match(regExp);
                    match = !!match || genos.gene && genos.gene.match(regExp);
                    match = !!match || genos.dbsnp && genos.dbsnp.match(regExp);
                    match = !!match || genos.gene && genos.gene.match(regExp);
                    match = !!match || genos.genobody && genos.genobody.match(regExp);
                    match = !!match || genos.rstext && genos.rstext.match(regExp);
                    match = !!match || genos.genosummary && genos.genosummary.match(regExp);

                    if(match) {
                        searchCollection.push(genos);
                    }
                }

                self.safeApply(self.scope, function() {
                    self.collectionService.setSearchCollection(searchCollection, true);
                });

            }, 100)();
        } else {
            self.collectionService.setFilteredCollection();
        }
    };

};

GenosController.prototype.setUpNoMatch = function() {
    var self = this;

    self.scope.$watchCollection(function(){ return self.collectionService.getActiveCollection()}, function(values) {
        self.noMatch = (values && !values.length);
    });
};

//resets the index
GenosController.prototype.setUpReset = function() {
    var self = this;

    self.reset = function() {
        self.filterService.clear(function() {
            window.location.reload();
        });
    };
};


//sets up alternate view types
GenosController.prototype.setUpViewTypes = function() {
    var self = this,
        printTableStyle;

    self.viewIndex = 0;
    self.safeApply(self.scope, function() {
        self.startTable = false;
        self.doShowGraph = false;
    });
    self.viewTypes = [
        {title: 'Block', type: 'block', mount: function() {
            if (self.doShowGraph) {
                return;
            }
            if (document.querySelector("#print-table")) {
                printTableStyle = document.querySelector("#print-table");
                printTableStyle.parentNode.removeChild(printTableStyle);
            }
            self.mountBlockView();
        }},
        {title: 'Table', type: 'table', mount: function() {
            if (self.doShowGraph) {
                return;
            }
            self.safeApply(self.scope, function() {
                self.startTable = true;
            });
            //Set landscape mode only
            if (!document.querySelector("#print-table")) {
                printTableStyle = document.createElement("style");
                printTableStyle.id = 'print-table';
                printTableStyle.innerHTML = "@page {size: landscape}";
                document.body.appendChild(printTableStyle);
            }
            // self.disableBlockView();
            self.scope.$broadcast('disableBlockView');
            if(self.mountTableView)
                self.mountTableView();
        }}
    ];

    self.showView = function(type, index) {
        if(!self.doShowGraph) {
            document.querySelector('.view-loader').style.visibility = 'visible';
        }

        for(var i=0, vL = self.viewTypes.length; i < vL; i++) {
            self.viewIndex = index;
            if(self.viewTypes[i].type === type) {
                window.setTimeout(function() {
                    this.viewTypes[i].mount();
                }.bind(self), 100);
                break;
            }
        }
    };
};

GenosController.prototype.hideLoading = function() {
    var self = this;

    var dialog = document.querySelector('.preloader-dialog');
    dialog.style.display = 'none';

    setTimeout(function() {
        self.safeApply(self.scope, function() {
            self.loading = false;
        });
    }, 100);
};

GenosController.prototype.hideWelcomeScreen = function () {
    var self = this;

    self.safeApply(self.scope, function() {
        self.closeWelcomeScreen = true;
    });
};

GenosController.prototype.setUpBoring = function() {
    var self = this,
        boringIndex;

    self.resetBoring = function() {
        self.boring = 'local';
        self.boringLoader = false;
        self.boringError = false;
    };

    self.setBoring = function() {
        self.collectionService.setBoring(boringIndex);

        //broadcast to blockview
        self.scope.$broadcast('boringUpdate', boringIndex);

        // for remote change
        if(self.boring === 'remote' &&
            (!self.boringInput ||  !self.boringInput.match(/YES/))) {
            self.boringError = 'Please enter the correct Value.';
        }
        else if(self.boring === 'remote' && self.boringInput.match(/YES/)) {
            self.boringLoader = true;
            self.boringError = false;

            self.postBoring(self.boringTitle).then(function() {
                self.boringLoader = false;
                self.showBoringOverlay = false;
            }, function() {
                self.boringLoader = false;
                self.boringError = 'Request Failed. Please Try Again.';
            });
        }
        else {
            self.showBoring = false;
        }
    };

    self.scope.$on('showBoring', function(event, index) {
        boringIndex = index;
        self.boringTitle = self.collectionService.getDefaultCollection()[index].title;
        self.showBoring = true;
    });
};

GenosController.prototype.setUpCount = function() {
    this.count = this.collectionService.getActiveCollection().length;
};

GenosController.$inject = ['$scope', 'collectionService', 'safeApply', 'sortTypes', 'filterService', 'postBoring', 'debounce'];
