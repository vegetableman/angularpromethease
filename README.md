Build notes for aws ec2-user

```
  sudo yum -y install git
  git clone git@bitbucket.org:cariaso/vegetableman_promethease_reports.git
  cd vegetableman_promethease_reports/
  cd ..


  sudo yum install -y gcc-c++ make openssl-devel 
  git clone git://github.com/joyent/node.git
  cd node/
  git tag -l
  git checkout v0.11.13
  ./configure 
  make
  sudo make install
  cd ..

  wget https://npmjs.org/install.sh
  sudo env PATH=$PATH sh install.sh 
  cd vegetableman_promethease_reports/
  npm install
  sudo env PATH=$PATH npm install bower -g --save-dev
  bower install
  sudo env PATH=$PATH npm install grunt-cli -g --save-dev
  grunt
```


** Changes **

* Complete rewrite to make it more modular.
* Pagination support based on discourse.com with a textfield that support jump navigation.
* Added preloader
* Header shifts up and down based on scroll to allow more viewing space.
* Clinvar mode added
* Separate print.css for table.Table could now be printed.There's a show all button that will display the whole table.
* Reactjs for rendering block and table view. angular just is not meant for such humungous data.
* Added a basic doughnut chart. 
* Js is now compressed. Bringing it's size to 208kb.
* Ui Changes to Graph view. Since, it's not entirely independent, has been made a separate button instead of a mode.
* Safari browser support added.
* Sorting is with quick sort. bringing it from 350ms to 100ms.
* IE fix
* Cheers.


** Please Note - Preloader Change ** (am available on skype):-

In order for the preloader to work, the script tag with mygenos array must be divided in to multiple script tags( which 
you can checkout on report-ui.html file) and the variable total above setPreloaderPerc is the total count of genotypes present 
in the report which must be set from the server on to the jinja2 template.

So, it would be:- 

```
var total = {{total}}
```

And the function setPreloaderPerc must be called at the end of each script tag. 
setPreloaderPerc has the logic to show the percentage based on the number of items already downloaded.

** So why divide the script tag? ** 

It's because the browser runs javascript only after the entire script content is downloaded. so just adding that function call on multiple places on the script does not work, as it would only be called after the entire script content is downloaded. nullifying the whole purpose fo a preloader.

Division allows progressive download and execution. so, the function will run at the end of each download.
