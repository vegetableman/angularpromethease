module.exports = function(grunt) {
  grunt.initConfig({
    browserify: {
      js: {
        // A single entry point for our app
        src: './app/js/app.js',
        // Compile to a single file to add a script tag for in your HTML
        dest: './dist/js/app.js',
      },
      //enables source maps
      options: {
        transform:  [require('grunt-react').browserify], //compile jsx to js
        browserifyOptions: {
          debug: true
        }
      }
    },
    copy: {
      all: {
        // This copies all the html and css into the dist/ folder
        expand: true,
        cwd: 'app/',
        src: ['**/*.html', '**/*.css'],
        dest: 'dist/',
      },
    },
    watch: {
      scripts: {
        files: ['Gruntfile.js', 'app/**/*.js', 'app/**/*.jsx', 'vendor_libs/**/*.js', '**/*.html', '**/*.css'],
        tasks: ['copy', 'concat', 'cssmin:css', 'cssmin:print', 'browserify']
      }
    },
    ngAnnotate: {
      app: {
        src: './dist/js/app.js',
        dest: './dist/js/app.js'
      }
    },
    uglify: {
      target: {
        options: {
          sourceMap: true,
          mangle: false
        },
        files: {
          './dist/js/app.min.js': './dist/js/app.js'
        }
      }
    },
    concat: {
      css: {
        src: ['./app/css/main.css', './app/css/normalize.css'],
        dest: './dist/css/build.css'
      }
    },
    cssmin: {
      css:{
        src: './dist/css/build.css',
        dest: './dist/css/build.min.css'
      },
      print:{
        src: './app/css/print.css',
        dest: './dist/css/print.min.css'
      }
    },
    exec: {
	   build_smalltest: {
    	 command: 'node build.js > dist/smalltest.html',
	     stdout: true
      }
    }
  });

  // Load the npm installed tasks
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-css')
  grunt.loadNpmTasks('grunt-react');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-exec');

  // The default tasks to run when you type: grunt
  grunt.registerTask('default', ['browserify', 'copy', 'ngAnnotate', 'uglify', 'concat', 'cssmin:css', 'exec']);
  grunt.registerTask('build', ['ngAnnotate', 'uglify', 'concat', 'cssmin:css']);
  grunt.registerTask('build-css', ['concat', 'cssmin:css']);
};
